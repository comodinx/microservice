'use strict';

const HOST = process.env.HOST;
const PORT = process.env.PORT || 8000;

module.exports = {
    host: HOST,
    port: PORT
};
