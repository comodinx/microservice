'use strict';

const { Op } = require('sequelize');

const NAME = process.env.DB_NAME || 'database';
const HOST_WRITE = process.env.DB_HOST_WRITE || process.env.DB_HOST || 'localhost';
const PORT_WRITE = process.env.DB_PORT_WRITE || process.env.DB_PORT || 3306;
const USER_WRITE = process.env.DB_USER_WRITE || process.env.DB_USER || 'username';
const PASS_WRITE = process.env.DB_PASS_WRITE || process.env.DB_PASS || null;
const HOST_READ = process.env.DB_HOST_READ || process.env.DB_HOST || 'localhost';
const PORT_READ = process.env.DB_PORT_READ || process.env.DB_PORT || 3306;
const USER_READ = process.env.DB_USER_READ || process.env.DB_USER || 'username';
const PASS_READ = process.env.DB_PASS_READ || process.env.DB_PASS || null;

const TIMEZONE = process.env.DB_TIMEZONE || '+00:00';

const POOL_ENABLED = ['1', 'true'].includes(String(process.env.DB_POOL_ENABLED).toLowerCase());
// Maximum number of connection in pool
const POOL_MAX = process.env.DB_POOL_MAX_CONN || 10;
// Minimum number of connection in pool
const POOL_MIN = process.env.DB_POOL_MIN_CONN || 0;
// The maximum time, in milliseconds, that a connection can be idle before being released
const POOL_IDLE = process.env.DB_POOL_IDLE || 10000;
// The maximum time, in milliseconds, that pool will try to get connection before throwing error
const POOL_ACQUIRE = process.env.DB_POOL_ACQUIRE || 60000;
// The time interval, in milliseconds, after which sequelize-pool will remove idle connections.
const POOL_EVICT = process.env.DB_POOL_EVICT || 1000;

let pool;

if (POOL_ENABLED) {
    pool = {
        max: parseInt(POOL_MAX),
        min: parseInt(POOL_MIN),
        idle: parseInt(POOL_IDLE),
        acquire: parseInt(POOL_ACQUIRE),
        evict: parseInt(POOL_EVICT)
    };
}

module.exports = {
    database: NAME,
    dialect: 'mysql',
    logging: false,
    timezone: TIMEZONE,
    replication: {
        read: [{
            host: HOST_READ,
            port: PORT_READ,
            username: USER_READ,
            password: PASS_READ,
        }],
        write: {
            host: HOST_WRITE,
            port: PORT_WRITE,
            username: USER_WRITE,
            password: PASS_WRITE,
        }
    },
    dialectOptions: {
        decimalNumbers: true
    },
    readOptions: {
        tiny: true
    },
    writeOptions: {
        useMaster: true,
        tiny: true
    },
    operatorsAliases: {
        ee: Op.eq
    },
    pool
};
