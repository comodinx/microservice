'use strict';

const https = require('https');
const config = require('../config');

function listen (app) {
    const env = process.env.NODE_ENV || 'development';

    if (env === 'test') {
        return;
    }

    let server = app;

    if (config('server.https.enabled', false)) {
        const options = {
            key: config('server.https.key'),
            cert: config('server.https.cert')
        };

        if (!options.key) {
            throw new Error('server.https.key not found. Please set in "config/server.js" the property "https.key" with private key content');
        }
        if (!options.cert) {
            throw new Error('server.https.cert not found. Please set in "config/server.js" the property "https.cert" with certificate.pem content');
        }
        server = https.createServer(options, app);
    }

    const port = config('server.port');
    const host = config('server.host');
    const callback = () => console.log(config('server.message', `Application running on${host ? `HOST ${host}` : ''} PORT ${port} in ${env} environment!`));

    if (host) {
        return server.listen(port, host, callback);
    }
    return server.listen(port, callback);
}

module.exports = listen;
