'use strict';

const app = require('./app');
const listen = require('./listen');
const middlewares = require('./middlewares');

module.exports = {
    app,
    listen,
    middlewares
};
