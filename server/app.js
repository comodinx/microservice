'use strict';

// Load configuration
require('../config');

const cors = require('cors');
const morgan = require('morgan');
const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const compression = require('compression');
const stream = require('../helpers/logger');
const middlewares = require('../server/middlewares');

const app = express();

const morganOptions = {
    stream,
    skip: req => process.env.NODE_ENV === 'test' || req.originalUrl.startsWith(process.env.MICROSERVICE_HEALTH_PATH || process.env.SERVER_HEALTH_PATH || '/health')
};

// Middlewares

// Check if microservice need support for multipart requests
if (['1', 'true'].includes(String(process.env.SERVER_MULTIPART).toLowerCase())) {
    app.use(middlewares.multipart);
}

// Morgan access log
app.use(morgan(!['0', 'false'].includes(String(process.env.LOGGER_PRETTY).toLowerCase()) ? 'dev' : 'tiny', morganOptions));

// Enable X-Fordered-For
app.enable('trust proxy');
// Disable X-Powered-By express
app.disable('x-powered-by');

// Enabled CORS support
app.use(cors());
// Enabled all CORS
app.options('*', cors());

// Enabled compression
app.use(compression());

// Add support for cookies parser
app.use(cookieParser());

// Add support for body parser
app.use(bodyParser.raw({
    limit: process.env.BODY_PARSER_LIMIT || process.env.BODY_PARSER_RAW_LIMIT || '5mb'
}));
app.use(bodyParser.text({
    limit: process.env.BODY_PARSER_LIMIT || process.env.BODY_PARSER_TEXT_LIMIT || '5mb'
}));
app.use(bodyParser.json({
    limit: process.env.BODY_PARSER_LIMIT || process.env.BODY_PARSER_JSON_LIMIT || '5mb'
}));
app.use(bodyParser.urlencoded({
    limit: process.env.BODY_PARSER_LIMIT || process.env.BODY_PARSER_URLENCODED_LIMIT || '5mb',
    extended: true
}));

module.exports = app;
