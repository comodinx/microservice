'use strict';

const P = require('bluebird');
const errors = require('./errors');
const health = require('./health');

class Route {
    constructor (options = {}) {
        this.options = options;
    }

    handle (req, res, next) {
        return P.bind(this)
            .then(() => this.initialize(req))
            .then(() => this.validate(req, res))
            .then(() => this.authorize(req, res))
            .then(() => this.handler(req, res))
            .then(result => this.success(res, result))
            .catch(error => this.error(error, req, res, next));
    }

    initialize (req) {
        return P.resolve(req);
    }

    validate () {
        return P.resolve();
    }

    authorize () {
        return P.resolve();
    }

    handler () {
        return P.resolve({});
    }

    success (res, result, statusCode = 200) {
        // Handle exceptional cases
        if (result && result.statusCode && result.description === 'already-response') {
            statusCode = result.statusCode;
            result = result.response;
        }
        return res.status(statusCode).send(result);
    }

    error (err, req, res, next) {
        return next(err);
    }

    handlerize () {
        return this.handle.bind(this);
    }
}

Route.errors = errors;
Route.health = health;

module.exports = Route;
