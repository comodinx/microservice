'use strict';

const _ = require('lodash');
const P = require('bluebird');
const async = require('async');
let Service;

function handle (app, options = {}) {
    (options.router || app).get(options.url || options.path || '/health', (req, res) => {
        const query = req.query || {};
        const _with = (query.with || '').split(',');
        const context = {
            req,
            res
        };
        const data = {
            alive: true
        };
        let p = P.resolve();

        // Source code information.
        if (options.rootDir) {
            // Package JSON exists always.
            options.pkg = options.pkg || require(`${options.rootDir}/package.json`);

            // TAG JSON is optional.
            try {
                options.tag = options.tag || require(`${options.rootDir}/tag.json`);
            } catch (e) { // eslint-disable-line brace-style
                // Ignore error when tag.json file not exist.
            }
        }

        // Basic package information.
        if (options.pkg) {
            data.name = options.pkg.name;
            data.description = options.pkg.description;
            data.version = options.pkg.version;
        }

        // Environment information.
        data.environment = process.env.NODE_ENV || 'production';

        // Source code information.
        if (query.tag && options.tag) {
            data.tag = options.tag;
        }

        // Database is alive?
        const checkDatabase = _with.includes('database') || _with.includes('*');

        if (checkDatabase && options.database && options.database.isAlive) {
            p = p.then(() => {
                return options.database
                    .isAlive()
                    .then(alive => {
                        data.database = {
                            alive: !!alive
                        };
                        return P.resolve(data);
                    })
                    .catch(error => {
                        data.database = {
                            alive: false,
                            error
                        };
                        return P.resolve(data);
                    });
            });
        }

        // Services
        const checkServices = _with.includes('dependencies') || _with.includes('dependencies-deep') || _with.includes('*');
        const checkServicesDeep = _with.includes('dependencies-deep') || _with.includes('*');

        if (checkServices && options.services && options.services.length) {
            const excludeServices = `${(query.excludeServices || query.exclude_services || '')}`.split(',');

            // Lazy initialize Service module
            if (!Service) {
                Service = require('../../helpers/service');
            }

            // Create dependencies data health if necesary
            data.dependencies = data.dependencies || {
                alive: true,
                list: []
            };

            p = p.then(() => {
                return new P(resolve => {
                    async.each(options.services, (service, next) => {
                        // Exclude service
                        if (excludeServices.includes(service)) {
                            data.dependencies.list.push({ id: service, exclude: true });
                            return P.resolve(next());
                        }

                        // Check service dependencie
                        const opts = {
                            params: {
                                excludeServices: [...options.services, ...excludeServices].join(',')
                            }
                        };

                        // Anidate dependecies
                        if (checkServicesDeep) {
                            opts.params.with = 'dependencies-deep';

                            if (checkDatabase) {
                                opts.params.with += 'database';
                            }
                        }

                        return Service[service]
                            .health(opts)
                            .then(res => {
                                // Handle response success
                                res.id = service;
                                res.alive = true;
                                data.dependencies.list.push(res);
                                return P.resolve(next());
                            })
                            .catch(error => {
                                // Handle response error
                                data.dependencies.alive = false;
                                data.dependencies.list.push({
                                    id: service,
                                    alive: false,
                                    error
                                });
                                return P.resolve(next());
                            });
                    }, error => {
                        // Microservice is alive, if dependecies are alive.
                        data.alive = data.dependencies.alive;

                        if (error) {
                            data.dependencies = data.dependencies || {};
                            data.dependencies.error = error;
                        }
                        return resolve(data);
                    });
                });
            });
        }

        // Any other custom health check
        if (_.isFunction(options.custom)) {
            p = p.then(() => {
                return options.custom(data, context);
            });
        }

        return p.then(() => {
            return res.send(data);
        });
    });
}

module.exports = handle;
