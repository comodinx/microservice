'use strict';

const _ = require('lodash');
const errors = require('../../errors');
const logger = require('../../helpers/logger');

const statuses = _.filter(_.map(errors, 'code'), _.identity);

const DEFAULT_ERROR_MESSAGE = 'Internal Server Error';
const DEFAULT_ERROR_STATUS = errors.InternalServerError.code;

function handle (app) {
    // Handle 404
    app.use((req, res, next) => {
        res.status(errors.NotFound.code).send((new errors.NotFound('Not found')).toJson());
    });

    // Handle uncached errors
    app.use((error, req, res, next) => {
        if (error instanceof errors.CustomError) {
            if (!error.code || !_.isNumber(error.code)) {
                error.code = DEFAULT_ERROR_STATUS;
            }
            return res.status(error.code).send(error.toJson());
        }

        const message = (error && error.message) || DEFAULT_ERROR_MESSAGE;
        let status = Number((error && (error.status || error.statusCode || error.responseCode || error.code)) || DEFAULT_ERROR_STATUS);

        // Check if status is a valid http status code
        if (!statuses.includes(status)) {
            status = DEFAULT_ERROR_STATUS;
        }

        logger.error(`${DEFAULT_ERROR_MESSAGE}. ${error} ${process.env.LOG_ERROR_STACK ? ((error && error.stack) || '') : ''}`);
        return res.status(status).send((new errors.CustomError(message, status)).toJson());
    });
}

module.exports = handle;
