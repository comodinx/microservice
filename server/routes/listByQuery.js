'use strict';

const _ = require('lodash');
const P = require('bluebird');
const Base = require('../routes');
const errors = require('../../errors');
const database = require('../../database');
const { query, logger, operators, service } = require('../../helpers');
const { Query } = require('@comodinx/sequelize');

const omitOptions = ['attributes', 'where', 'order', 'group', 'limit', 'offset'];
const defaultOptions = {
    logger: false,
    service: false
};

class Route extends Base {
    /**
     * Route constructor
     */
    constructor (model, options) {
        super(_.defaultsDeep({}, options || {}, defaultOptions));
        this.model = model;

        if (!this.model || !_.isString(this.model)) {
            throw new errors.NotImplemented('Model name not setting on route options');
        }
    }

    /**
     * Handle request
     */
    handler (req) {
        const context = {
            logger: req.logger || logger,
            service: req.service || service,
            model: database.models[this.model],
            ...(req.params || {}),
            ...(req.query || {})
        };
        const options = query.parse(req.query, {
            pageSize: this.options.defaultPageSize || 10,
            mapKeyParse: this.options.mapKeyParse,
            jsonKeys: this.options.defaultJsonKeys || ['metadata']
        });

        context.parser = query.filters.lastParser;
        context.hasFilters = !!options.where;

        // Setup proxies, only if exist
        options.logger = context.logger;
        options.service = context.service;

        if (!context.model) {
            throw new errors.InternalServerError(`Model name [${this.model}] not found on database models`);
        }

        context.modelTable = context.model.options.nomenclature.table;

        return this.list(options, context);
    }

    /**
     * List models
     */
    list (options, context) {
        if (!options.where) {
            // As we do not have search conditions to filter, it is not necessary to filter with relationships
            return context.model.find(options).then(models => models || []);
        }

        // Prepare query
        context.query = context.query || {};

        // Prepare relationships promises
        const promises = [];

        _.each(this.options.relationships, relationship => {
            if (!relationship.remote) {
                promises.push(this.prepareRelationshipJoin(context, options, relationship));
            }
            else {
                promises.push(this.prepareRelationshipService(context, options, relationship));
            }
        });

        // For filter and paginate, it is necessary to obtain the accounts and people that match the search.
        return P.all(promises)
            .then(() => this.prepareRelationshipWhere(context, options))
            .then(() => this.findByQuery(context, options));
    }

    /**
     * Prepare JOIN and WHERE conditions
     */
    prepareRelationshipJoin (context, options, relationship) {
        const opts = _.isString(relationship) ? { name: relationship } : relationship;
        // Relationship name
        const name = opts.name;
        // Relationship table
        const table = opts.table || `${context.model.options.nomenclature.relationship}_${name}`;
        // Relationship table source field
        const fieldSource = opts.fieldSource || `${name}.id_${context.model.options.nomenclature.relationship}`;
        // Relationship table target field
        const fieldTarget = opts.fieldTarget || `${context.modelTable}.id`;
        // Filter keys on where
        const keys = _.filter(_.keys(options.where), key => _.startsWith(key, `${name}.`));

        // Check if need filter this relationship
        if (!keys.length) {
            return P.resolve(context);
        }

        const filters = _.pick(options.where, keys);

        // Verify filters content
        if (!filters || !Object.keys(filters).length) {
            return P.resolve(context);
        }

        options.where = _.omit(options.where, keys);
        context.query.join = context.query.join || [];
        context.query.join.push({
            table,
            alias: name,
            on: {
                [fieldSource]: fieldTarget
            }
        });
        context.query.where = context.query.where || {};

        _.each(filters, (condition, key) => {
            // Prepare content keys, includes symbols keys.
            const keys = Object.getOwnPropertySymbols(condition);

            // Each all keys.
            _.each(keys, operator => {
                context.query.where[key] = context.query.where[key] || {};
                context.query.where[key][operators[operator] || operator] = condition[operator];
            });
        });

        return P.resolve(context);
    }

    /**
     * Prepare WHERE conditions for remote relationships
     */
    prepareRelationshipService (context, options, relationship) {
        const opts = relationship;
        // Relationship name
        const name = opts.name;
        // Relationship service
        const service = opts.service || name;
        // Relationship url
        const uri = opts.uri || opts.url || `/${name}`;
        // Relationship remote service source field
        const fieldSource = opts.fieldSource || `id_${context.model.options.nomenclature.relationship}`;
        // Relationship local table target field
        const fieldTarget = opts.fieldTarget || `${context.modelTable}.id`;
        // Extra relationship service options
        const extraOptions = opts.options || {};
        // Filter keys on where
        const keys = _.filter(_.keys(options.where), key => _.startsWith(key, `${name}.`));

        // Save flags for this relationship
        context.remoteRelationships = context.remoteRelationships || {};
        context.remoteRelationships[name] = context.remoteRelationships[name] || {};
        context.remoteRelationships[name].hasFilters = keys.length > 0;

        // Check if need filter this relationship
        if (!context.remoteRelationships[name].hasFilters) {
            return P.resolve(context);
        }

        // Take only filters starts with "<relations name>."
        const filters = _.mapKeys(_.pick(options.where, keys), (condition, key) => key.replace(`${name}.`, ''));
        // Subtract filters taked on previous line
        options.where = _.omit(options.where, keys);

        const request = _.merge({}, {
            uri,
            qs: {
                tiny: true,
                fields: fieldSource,
                filters: context.parser.format(filters),
                pageSize: 100000,
                page: 1
            }
        }, extraOptions);

        return context.service[service]
            .get(request)
            .then(models => {
                // Save has models flag for this relationship
                context.remoteRelationships[name].hasModels = models && models.length;

                // Prepare where for this relationship
                this.prepareRelationshipServiceWhere(context, options, {
                    name,
                    models,
                    fieldSource,
                    fieldTarget
                });

                // Return context :~
                return context;
            })
            .catch(error => {
                context.logger.warn(`Error on find ${context.modelTable} relationship [${name}]. ${error}`);
                context.remoteRelationships[name].hasModels = false;
                return context;
            });
    }

    /**
     * Prepare WHERE conditions for remote relationships
     */
    prepareRelationshipServiceWhere (context, options, result) {
        const { name, models, fieldSource, fieldTarget } = result;

        // Check if is necesary prepare where for this relationship remote service
        if (!models || !models.length) {
            return options;
        }

        // Prepare where for this relationship remote service
        const ids = new Set();
        let current;

        _.each(models, model => ids.add(model[fieldSource]));

        // Check if is union for use OR condition
        if (context.union) {
            // Concatenator operator OR
            const operator = operators[database.Op.or];

            options.where[operator] = options.where[operator] || [];
            current = options.where[operator].find(condition => condition[fieldTarget]);

            if (!current) {
                current = { [fieldTarget]: {} };
                options.where[operator].push(current);
            }

            // Conditional operator IN
            current[fieldTarget][database.Op.in] = current[fieldTarget][database.Op.in] || [];
            current[fieldTarget][database.Op.in] = current[fieldTarget][database.Op.in].concat(Array.from(ids));
        }
        // Use AND condition
        else {
            current = options.where[fieldTarget] = options.where[fieldTarget] || {};

            // Conditional operator IN
            current[database.Op.in] = current[database.Op.in] || [];
            current[database.Op.in] = current[database.Op.in].length ? _.intersection(current[database.Op.in], Array.from(ids)) : current[database.Op.in].concat(Array.from(ids));

            if (!current[database.Op.in].length) {
                context.remoteRelationships[name].hasModels = false;
            }
        }
        return options;
    }

    /**
     * Prepare WHERE conditions for current TABLE
     */
    prepareRelationshipWhere (context, options) {
        let where = context.query.where = context.query.where || {};
        const or = operators[database.Op.or].trim();

        if (context.union) {
            // Initialize OR condition, if necesary.
            where = _.cloneDeep(where || {});
            where = where[or] = where[or] || {};
            _.each(context.query.where, (condition, key) => (where[key] = condition));
        }

        const statements = [...Object.getOwnPropertySymbols(options.where), ...Object.getOwnPropertyNames(options.where)];

        _.each(statements, key => {
            const condition = options.where[key];

            // Handle array conditions. (OR)
            if (_.isArray(condition)) {
                // For each OR conditions.
                _.each(condition, conditions => {
                    // Handle sequelize where object conditions.
                    if (conditions && conditions.logic && conditions.comparator && conditions.attribute) {
                        // Prepare content keys, includes symbols keys.
                        const keys = Object.getOwnPropertySymbols(conditions.logic);

                        _.each(keys, operator => {
                            where[`${context.modelTable}.${conditions.attribute.val}`] = where[`${context.modelTable}.${conditions.attribute.val}`] || {};
                            where[`${context.modelTable}.${conditions.attribute.val}`][operators[operator] || operator] = conditions.logic[operator];
                        });
                    }
                    // Handle other object conditions.
                    else {
                        const fields = Object.keys(conditions);

                        // Each all keys.
                        _.each(fields, field => {
                            const orCondition = conditions[field];

                            // Prepare content keys, includes symbols keys.
                            const keys = Object.getOwnPropertySymbols(orCondition);

                            _.each(keys, operator => {
                                where[`${context.modelTable}.${field}`] = where[`${context.modelTable}.${field}`] || {};
                                where[`${context.modelTable}.${field}`][operators[operator] || operator] = orCondition[operator];
                            });
                        });
                    }
                });
            }
            // Handle object condition (AND)
            else if (_.isObject(condition)) {
                // Prepare content keys, includes symbols keys.
                const keys = Object.getOwnPropertySymbols(condition);

                // Each all keys.
                _.each(keys, operator => {
                    where[`${context.modelTable}.${key}`] = where[`${context.modelTable}.${key}`] || {};
                    where[`${context.modelTable}.${key}`][operators[operator] || operator] = condition[operator];
                });
            }
        });

        // Reasigned, because union need clone where object
        context.query.where = context.union ? { or: where } : where;

        return P.resolve(context);
    }

    /**
     * Find models by query
     */
    findByQuery (context, options) {
        return P.bind(this)
            .then(() => this.checkConditions(context, options))
            .then(isNeedFind => {
                // Check if is necesary find models
                if (!isNeedFind) {
                    return P.resolve([]);
                }

                return P.bind(this)
                    // Build query with builded conditions
                    .then(() => this.builQuery(context, options))
                    // Find model on database with query string
                    .then(query => database.query(query, { type: database.QueryTypes.SELECT }))
                    // Normalize models
                    .then(models => {
                        if (!models || !models.length) {
                            return models;
                        }

                        const opts = context.model.prepareReadOptions(options);

                        if (!opts.populate || opts.unfilled) {
                            return models;
                        }
                        return context.model.mappingAll(models || [], _.omit(opts, omitOptions));
                    });
            });
    }

    /**
     * Check conditions
     */
    checkConditions (context, options) {
        // Check if is necesary find by intersection strategy. we respect any conditions
        if (!context.union && context.hasFilters) {
            // Check condition without results
            if (!_.every(context.remoteRelationships, relationship => !relationship.hasFilters || (relationship.hasFilters && relationship.hasModels))) {
                return P.resolve(false);
            }
            if (context.query.where && (Object.getOwnPropertySymbols(context.query.where).length + Object.getOwnPropertyNames(context.query.where).length) === 0) {
                return P.resolve(false);
            }
        }
        return P.resolve(true);
    }

    /**
     * Build query
     */
    builQuery (context, options) {
        const query = Query.select(context.modelTable, {
            fields: [`${context.modelTable}.*`],
            join: context.query.join,
            where: context.query.where,
            order: options.order,
            group: options.group,
            limit: options.limit,
            offset: options.offset
        });

        return P.resolve(query);
    }
}

module.exports = Route;
