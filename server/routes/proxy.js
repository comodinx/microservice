'use strict';

const _ = require('lodash');
const P = require('bluebird');
const Base = require('../routes');
const errors = require('../../errors');
const { logger, service } = require('../../helpers');

const defaultContentType = 'application/json';
const defaultOptions = {
    sameContentType: true
};

class Route extends Base {
    /**
     * Route constructor
     *
     * @param  String|Function  serviceName                       Service name defined by helpers/service.add
     * @param  String|Function  [serviceMethod=<request.method>]  Service method
     * @param  String|Function  [serviceUrl=<request.path>]       Service url
     * @param  Object|Function  [serviceOptions={}]               Service options
     * @param  Boolean          [sameContentType=true]            Indicate that response content type is same of service response content type
     */
    constructor (options = {}) {
        super({ ...defaultOptions, ...options });

        if (!this.options.serviceName || !_.isString(this.options.serviceName)) {
            throw new errors.NotImplemented('Service name not setting on route options');
        }
    }

    /**
     * Handle request
     */
    handler (req, res) {
        const context = {
            logger: req.logger || logger,
            service: req.service || service,
            params: req.params,
            query: req.query,
            body: req.body,
            req,
            res
        };

        return P.bind(this)
            .then(() => this.populate(context))
            .then(() => this.verify(context))
            .then(() => this.resolve(context))
            .then(() => this.call(context))
            .then(response => this.callSuccess(context, response))
            .catch(error => this.callError(error, context));
    }

    /**
     * Populate all need data.
     */
    populate () {
        return P.resolve();
    }

    /**
     * Verify all data.
     */
    verify () {
        return P.resolve();
    }

    /**
     * Resolve all need data (service name, url, method, etc).
     */
    resolve (context) {
        return P.all([
            this.resolver(context, 'serviceName'),
            this.resolver(context, 'serviceMethod', context.req.method.toLowerCase()),
            this.resolver(context, 'serviceUrl', context.req.path),
            this.resolver(context, 'serviceOptions', {})
        ]);
    }

    /**
     * Resolver option.
     */
    resolver (context, name, defaultValue) {
        let promise = P.resolve();

        context[name] = this.options[name] || defaultValue;

        // Resolve the option of the service, only in case of being function.
        if (_.isFunction(context[name])) {
            promise = promise
                .then(() => context[name](context))
                .then(result => {
                    context[name] = result;
                    return context;
                });
        }
        return promise;
    }

    /**
     * Call to remote service
     */
    call (context) {
        return context.service[context.serviceName][context.serviceMethod](this.prepareOptions(context));
    }

    /**
     * Handle call success
     */
    callSuccess (context, res) {
        if (this.options.sameContentType && res && res.headers) {
            // Response type is equal to service res content type
            context.res.type(res.headers['content-type'] || defaultContentType);
        }
        return res && res.data ? res.data : res;
    }

    /**
     * Handle call error
     */
    callError (error, context) {
        return P.reject(errors.from(error));
    }

    /**
     * Prepare options for call remote service
     */
    prepareOptions (context) {
        return _.merge({
            // Indicate that service response is raw type
            raw: !_.isNil(this.options.raw) ? this.options.raw : false,
            // Axion options
            uri: context.serviceUrl,
            body: context.body,
            qs: context.query
        }, context.serviceOptions || {});
    }
}

module.exports = Route;
