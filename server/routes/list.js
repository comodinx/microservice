'use strict';

const _ = require('lodash');
const Base = require('../routes');
const errors = require('../../errors');
const database = require('../../database');
const { query, logger, service } = require('../../helpers');

class Route extends Base {
    /**
     * Route constructor
     */
    constructor (model, options = {}) {
        super(options);
        this.model = model;

        if (!this.model || !_.isString(this.model)) {
            throw new errors.NotImplemented('Model name not setting on route options');
        }
    }

    /**
     * Handle request
     */
    handler (req) {
        const context = {
            logger: req.logger || logger,
            service: req.service || service,
            ...(req.params || {}),
            ...(req.query || {})
        };
        const options = query.parse(req.query, {
            pageSize: this.options.defaultPageSize || 10,
            mapKeyParse: this.options.mapKeyParse,
            jsonKeys: this.options.defaultJsonKeys || ['metadata']
        });

        // Setup proxies, only if exist
        options.logger = context.logger;
        options.service = context.service;

        return this.list(options, context);
    }

    /**
     * List models
     */
    list (options) {
        const model = database.models[this.model];

        if (!model) {
            throw new errors.InternalServerError(`Model name [${this.name}] not found on database models`);
        }

        return model.find(options).then(models => models || []);
    }
}

module.exports = Route;
