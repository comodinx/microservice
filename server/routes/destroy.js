'use strict';

const _ = require('lodash');
const P = require('bluebird');
const Base = require('.');
const errors = require('../../errors');
const database = require('../../database');
const { query, validator, service, logger } = require('../../helpers');

const defaultOptions = {
    paramNameId: 'id',
    validatorIdType: 'isInteger'
};

class Route extends Base {
    /**
     * Route constructor
     */
    constructor (model, options) {
        super(_.defaultsDeep({}, options || {}, defaultOptions));
        this.model = model;

        if (!this.model || !_.isString(this.model)) {
            throw new errors.NotImplemented('Model name not setting on route options');
        }
    }

    /**
     * Validate request
     */
    validate (req) {
        if (_.isEmpty(req.params)) {
            throw new errors.BadRequest('Bad request. Please set "id" parameter');
        }

        const id = req.params[this.options.paramNameId];
        const validatorIdType = this.options.validatorIdType;

        if (_.isFunction(validatorIdType) && !validatorIdType(id, req)) {
            throw new errors.BadRequest(`Bad request. Please use "${this.options.paramNameId}" on parameters`);
        }
        else if (_.isString(validatorIdType) && !validator[validatorIdType](id)) {
            throw new errors.BadRequest(`Bad request. Please use "${this.options.paramNameId}" on parameters`);
        }
    }

    /**
     * Handle request
     */
    handler (req) {
        const context = {
            logger: req.logger || logger,
            service: req.service || service,
            ...(req.params || {}),
            ...(req.query || {})
        };
        const options = query.parse(req.query);
        const id = req.params[this.options.paramNameId];

        // boolean query param to javascript type
        context.force = !!['TRUE', '1'].includes(req.query.force ? req.query.force.toUpperCase() : '');

        // Setup proxies, only if exist
        options.logger = context.logger;
        options.service = context.service;

        return P.bind(this)
            .then(() => this.get(id, options, context))
            .then(() => this.destroy(id, context))
            .then(() => {
                return {
                    description: 'already-response',
                    statusCode: errors.NoContent.code
                };
            });
    }

    /**
     * Get model by unique identifier ID
     */
    get (id, options) {
        const model = database.models[this.model];

        if (!model) {
            throw new errors.InternalServerError(`Model name [${this.name}] not found on database models`);
        }

        return model.getById(id, options).then(model => {
            if (!model) {
                throw new errors.NotFound(`Not found ${this.model} ${id}`);
            }
            return model;
        });
    }

    /**
     * Delete model by unique identifier ID
     */
    destroy (id, context) {
        const model = database.models[this.model];
        if (context.force) {
            // Destroy current model.
            return model.destroy(id);
        }
        else {
            // Inactivate currente model.
            return model.update({ active: 0 }, id);
        }
    }
}

module.exports = Route;
