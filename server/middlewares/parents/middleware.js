'use strict';

const P = require('bluebird');

function handle (handler) {
    return (req, res, next) => {
        return P.resolve()
            .then(() => handler(req, res))
            .then(() => P.resolve(next()))
            .catch(next);
    };
}

module.exports = handle;
