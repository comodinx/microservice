'use strict';

const _ = require('lodash');
const P = require('bluebird');
const Busboy = require('busboy');
const config = require('../../config');
const logger = require('../../helpers/logger');
const middleware = require('./parents/middleware');

const OPTIONS = _.defaultsDeep({}, config('form.multipart', {}), {
    limits: {
        file: 0,
        fileSize: 10485760
    }
});

function handle (req, res) {
    const contentType = req.get('content-type') || '';

    req.body = req.body || {};
    req.files = req.files || {};
    req.isMultipart = contentType.startsWith('multipart');

    if (!req.isMultipart || req.form) {
        return P.resolve();
    }

    return parse(req, {
        files: req.isMultipart
    });
}

function parse (req, options) {
    req.form = getForm(req, options);

    return new P((resolve, reject) => {
        req.form.on('field', getField.bind(null, req));

        if (options.files) {
            req.form.on('file', getFile.bind(null, req));
        }
        req.form.on('error', error => {
            reject(error);
        });
        req.form.on('finish', () => {
            resolve();
        });
        req.pipe(req.form);
    })
        .then(() => {
            req.body = req.form.fields;
            req.files = req.form.files;
            return P.resolve();
        });
}

function getForm (req, options) {
    const conf = _.extend(OPTIONS, {
        headers: req.headers
    });

    conf.limits = conf.limits || {};
    conf.limits.files = options.files || conf.limits.files || 0;

    return _.extend(new Busboy(conf), {
        fields: {},
        files: {}
    });
}

function getField (req, name, value) {
    _.set(req.form.fields, name, value);
}

function getFile (req, name, file, filename, encoding, contentType) {
    file.chunks = [];

    file.on('data', chunk => {
        file.chunks.push(chunk);
    });
    file.on('error', error => {
        logger.error(`${error} ${process.env.LOG_ERROR_STACK ? ((error && error.stack) || '') : ''}`);

        req.emit('error', {
            type: 'file'
        });
    });
    file.on('limit', () => {
        req.emit('limit', {
            type: 'file'
        });
    });
    file.on('end', () => {
        if (file.truncated) {
            return;
        }

        const buffer = Buffer.concat(file.chunks);

        req.form.files[name] = {
            value: buffer,
            options: {
                knownLength: buffer.length,
                filename,
                encoding,
                contentType
            }
        };
    });
}

module.exports = middleware(handle);
