'use strict';

const _ = require('lodash');
const errors = require('@comodinx/http-errors');

// constant for generic error
const INTERNAL_SERVER_ERROR = 500;

// default error options
const defaultFromOptions = {
    error: 'InternalServerError',
    message: 'Internal Server Error',
    code: INTERNAL_SERVER_ERROR
};

// errors list base on codes
const codes = _.reduce(errors, (result, error) => {
    if (error.code !== 500) {
        result[error.code] = error;
    }
    return result;
}, { 500: errors.InternalServerError });

// logger variable, used for lazy load logger module
let logger;

/**
 * Handle ""all"" error cases
 */
function from (error, options, debug = true) {
    options = _.defaultsDeep({}, options || {}, defaultFromOptions);

    //
    // Handle error in error
    //
    if (isErrorInError(error)) {
        return transformErrorInError(error, options, debug);
    }

    //
    // Handle axios errors
    //
    if (isAxiosError(error)) {
        return transformAxiosError(error, options, debug);
    }

    //
    // Handle other error cases
    //

    // Get the message
    const message = (error && error.message) || options.message;
    // Get the extra information
    const extra = _.merge({}, (error && error.extra) || {}, options.extra || {});

    // Check if not need customize error
    if (error instanceof errors.CustomError) {
        error.extra = {
            ...error.extra,
            ...extra
        };
        if (debug) errors.log(error, error.code);
        return error;
    }

    // Check if has error by name on errors
    if (error && errors[error.name]) {
        const errorByName = new errors[error.name](message, extra);
        if (debug) errors.log(error.stack ? error : errorByName, error.stack ? INTERNAL_SERVER_ERROR : errorByName.code);
        return errorByName;
    }

    // Check if has error by code on errors
    const errorByCode = new errors.CustomError(message, (error && (error.code || error.statusCode)) || options.code, extra);
    if (debug) errors.log(error.stack ? error : errorByCode, error.stack ? INTERNAL_SERVER_ERROR : errorByCode.code);
    return errorByCode;
}

/**
 * Indicate is error is an "error in error"
 */
function isErrorInError (error) {
    return (
        error &&
        error.error &&
        (_.isString(error.error.error) || _.isString(error.error.message)) &&
        (_.isNumber(error.error.code) || _.isNumber(error.error.status) || _.isNumber(error.error.statusCode))
    );
}

/**
 * Transform error in error on comodinx/error system
 */
function transformErrorInError (error, options, debug) {
    const realError = error.error;
    const message = realError.error || realError.message;
    const extras = {
        ...realError.extra,
        ...options.extra
    };

    return transformError(realError, message, extras, /* debugable error */ error.stack ? error : realError, debug);
}

/**
 * Indicate is error is an "axios error"
 */
function isAxiosError (error) {
    return (
        error &&
        error.response &&
        error.response.data &&
        (_.isString(error.response.data.error) || _.isString(error.response.data.message)) &&
        (_.isNumber(error.response.data.code) || _.isNumber(error.response.data.status) || _.isNumber(error.response.data.statusCode))
    );
}

/**
 * Transform axios error on comodinx/error system
 */
function transformAxiosError (error, options, debug) {
    const realError = error.response.data;
    const message = realError.error || realError.message;
    const extras = {
        ...realError.extra,
        ...options.extra,
        ...error.extra
    };

    return transformError(realError, message, extras, /* debugable error */ error.stack ? error : realError, debug);
}

/**
 * Transform axios error on comodinx/error system
 */
function transformError (error, message, extras, debugableError, debug) {
    // Handle error by code
    if (error.code && codes[error.code]) {
        // Log error if flag log is true
        if (debug) errors.log(debugableError, error.code);

        return new codes[error.code](message, extras);
    }

    // Handle error by status
    if (error.status && codes[error.status]) {
        // Log error if flag log is true
        if (debug) errors.log(debugableError, error.status);

        return new codes[error.status](message, extras);
    }

    // Handle error by statusCode
    if (error.statusCode && codes[error.statusCode]) {
        // Log error if flag log is true
        if (debug) errors.log(debugableError, error.statusCode);

        return new codes[error.statusCode](message, extras);
    }

    // Log error if flag log is true
    if (debug) errors.log(debugableError, error.code || error.status || error.statusCode || INTERNAL_SERVER_ERROR);

    return new errors.CustomError(message, error.code || error.status || error.statusCode || INTERNAL_SERVER_ERROR, extras);
}

/**
 * Log error
 */
function log (error, status) {
    if (!logger) {
        logger = require('../helpers/logger');
    }
    if (!logger.error) {
        return;
    }

    if (Number(status) !== INTERNAL_SERVER_ERROR) {
        return;
    }

    try {
        const message = (error && (error.stack || error.message || error.error || error)) || defaultFromOptions.message;
        const extra = ((error && error.extra) || null);

        logger.error(message, extra ? JSON.stringify(extra, null, 2) : '');
    }
    catch (e) {
        console.error('Error on try log on errors.from', status, error);
    }
}

//
// public functions
//
errors.from = from;
errors.log = log;
errors.isAxiosError = isAxiosError;
errors.isErrorInError = isErrorInError;

module.exports = errors;
