'use strict';

const _ = require('lodash');
const moment = require('moment');
const numeral = require('numeral');

const locale = process.env.MICROSERVICE_LOCALE || process.env.DEFAULT_LOCALE || 'es';

// Set default moment format.
moment.locale(process.env.MICROSERVICE_MOMENT_LOCALE || process.env.MOMENT_LOCALE || locale);
moment.defaultFormat = process.env.MOMENT_DEFAULT_FORMAT || 'YYYY-MM-DD HH:mm:ss';

// Set default numeral formats.
require(`numeral/locales/${process.env.NUMERAL_LOCALE || locale}`);
numeral.locale(process.env.NUMERAL_LOCALE || locale);
numeral.defaultIntegetFormat = process.env.NUMERAL_DEFAULT_INTEGER_FORMAT || '0,0';
numeral.defaultDecimalFormat = process.env.NUMERAL_DEFAULT_DECIMAL_FORMAT || '0,0.00';
numeral.defaultBalanceFormat = process.env.NUMERAL_DEFAULT_BALANCE_FORMAT || '$ 0,0.00';

// Set default lodash truncate options.
_.defaultTruncateOptions = {
    length: Number(process.env.LODASH_TRUNCATE_LENGTH || 24),
    separator: process.env.LODASH_TRUNCATE_SEPARATOR || ' '
};
