'use strict';

const FIXEABLE_ERROR = ' is not defined';
const MAX_FIXEABLE_ERRORS = 3;

/**
 * Parse a JSON from an untrusted string that might have some minor errors.
 * @param {String} json json string
 * @param {Integer} attemps attempts start counter. Default 0.
 * @param {Integer} maxFixeablesErrors max fix iterations. Default 3.
 * @returns {Object|undefined} return the parsed object or undefined if exceeds the attemps of fixable errors
 */
function jsonByEval (json, attemps = 0, maxFixeablesErrors = MAX_FIXEABLE_ERRORS) {
    try {
        // eslint-disable-next-line no-new-func
        return new Function(`return ${json}`)();
    }
    catch (e) {
        if (attemps >= maxFixeablesErrors) {
            console.error('Error on parse JSON by eval', e.message);
            return;
        }
        // Try to fix error.
        const msg = e.message;
        if (msg.endsWith(FIXEABLE_ERROR)) {
            const invalidValue = msg.replace(FIXEABLE_ERROR, '').trim();
            json = json.replace(invalidValue, `"${invalidValue}"`);
            return jsonByEval(json, ++attemps);
        }
        console.error('Error on parse JSON by eval', e.message);
    }
}

module.exports = jsonByEval;
