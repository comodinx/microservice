'use strict';

const _ = require('lodash');
const P = require('bluebird');
const axios = require('axios');
const ServiceAgent = require('service-agent');
const config = require('../config');
const errors = require('../errors');

const methods = ['get', 'post', 'put', 'patch', 'delete', 'options', 'head', 'connect', 'trace'];
const instances = {};
const mapKeys = {
    baseUrl: 'baseURL',
    uri: 'url',
    qs: 'params',
    body: 'data'
};
const defaultAddOptions = {
    responseType: 'json',
    debug: false
};

// Default service agent
const serviceAgent = config('services.userAgent', process.env.MICROSERVICE_SERVICE_AGENT || process.env.SERVICE_AGENT || '');

// Must be greater than requestTimeout
const connectionTimeout = config('services.timeout.connection', process.env.MICROSERVICE_SERVICE_CONNECTION_TIMEOUT || process.env.SERVICE_CONNECTION_TIMEOUT || 21000);
const requestTimeout = config('services.timeout.request', process.env.MICROSERVICE_SERVICE_REQUEST_TIMEOUT || process.env.SERVICE_REQUEST_TIMEOUT || 20000);

const connectionErrors = ['ECONNREFUSED', 'ENETUNREACH'];

class Service {
    constructor (options) {
        this.options = options;
        this.connectionTimeout = options.connectionTimeout || connectionTimeout;
        this.requestTimeout = options.requestTimeout || requestTimeout;
        this.r = axios.create(this.options);
    }

    request (method, options) {
        return new P((resolve, reject) => {
            const opts = this.prepareOptions(method, options);

            return this.r
                .request(opts)
                .then(res => resolve(opts.raw ? res : res.data))
                .catch(error => {
                    const extra = {
                        from: this.r.defaults.name,
                        path: opts.url,
                        method: opts.method
                    };

                    if (error.response) {
                        /*
                        * The request was made and the server responded with a
                        * status code that falls out of the range of 2xx.
                        * Convert axios error to @comodinx/http-errors
                        * Throw error to be handled outside this abstract method
                        */
                        error.extra = extra;
                        return reject(error);
                    }
                    else if (error.request) {
                        // The request was made but no response was received, `error.request`
                        if (connectionErrors.includes(error.code)) {
                            // no conection made
                            return reject(new errors.ServiceUnavailable('Service Unavailable. Try again later.', extra));
                        }
                        // timeout error
                        return reject(new errors.GatewayTimeout('Timeout Error. Try again later.', extra));
                    }
                    else {
                        if (error.__CANCEL__) {
                            // The service is unreachable (connection timeout)
                            return reject(new errors.ServiceUnavailable('Service Unavailable. Try again later.', extra));
                        }
                        // Something happened in setting up the request and triggered an Error
                        error.extra = extra;
                        return reject(new errors.InternalServerError('Internal Server Error', error));
                    }
                });
        })
            .catch(error => {
                throw errors.from(error);
            });
    }

    prepareOptions (method, options) {
        if (_.isString(options)) {
            options = {
                url: options
            };
        }

        // set default methods and headers
        options.method = method || 'get';
        options.headers = _.merge({}, options.headers || {});

        // set request timeout
        options.timeout = options.timeout || this.requestTimeout;

        // set of connection timeout cancel token
        const source = axios.CancelToken.source();
        const connectionTimeout = options.connectionTimeout || this.connectionTimeout;
        setTimeout(() => source.cancel(), connectionTimeout);
        options.cancelToken = source.token;

        return _.mapKeys(options, (value, key) => mapKeys[key] || key);
    }

    health (options = {}) {
        options.url = options.url || options.uri || this.options.health || '/health';
        return this.get(options);
    }

    getOne (options) {
        if (_.isString(options)) {
            options = {
                url: options
            };
        }

        options = options || {};

        if (options.qs) {
            options.params = options.qs;
        }

        options.params = options.params || {};
        options.params.pageSize = 1;
        options.params.page = 1;

        return this.get(options).then(models => {
            models = models || [];

            if (!_.isArray(models)) {
                models = [models];
            }
            return models;
        }).get(0);
    }
}

/**
  * Transform all http methods on service function
  *
  * service.get
  * service.post
  * service.put
  * service.patch
  * service.delete
  * service.options
  * service.head
  * service.connect
  * service.trace
  */
_.each(methods, method => {
    Service.prototype[method] = function (options) {
        return this.request(method, options);
    };
});

/**
  * Add read only service property.
  *
  * @param  Object  service  Options for service
  * @example
  *   service.add({
  *     baseURL: 'https://myservice.mydomain.com',
  *     name: 'myService',
  *     port: 8081
  *   })
  */
Service.add = function (options = {}) {
    options = { ...defaultAddOptions, ...options };

    // Check if need add service, verify options with name.
    if (!options.name) {
        return;
    }

    // Add service with options.name
    Object.defineProperty(Service, options.name, {
        configurable: true,
        enumerable: true,
        get: function () {
            let instance = instances[options.name];

            if (!instance) {
                if (!options.httpAgent) {
                    options.httpAgent = new ServiceAgent({
                        service: serviceAgent
                    });
                }
                instance = instances[options.name] = new Service(options);
            }
            return instance;
        }
    });
};

/**
  * Add support for all services configured on configuration
  */
_.each(_.merge({}, config('service', {}), config('services', {})), service => Service.add(service));

module.exports = Service;
