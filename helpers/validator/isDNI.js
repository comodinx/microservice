'use strict';

const _ = require('lodash');

const regexNumber = /^\d+$/;

/**
 * Returns whether a DNI (Documento Nacional de Identidad) is valid.
 */
function validate (value) {
    if (!_.isString(value) || value.length < 7 || value.length > 8) {
        return false;
    }
    return regexNumber.test(value);
}

module.exports = validate;
