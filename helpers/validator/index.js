'use strict';

const validator = require('validator');

validator.isInteger = require('./isInteger');
validator.isNumber = require('./isNumber');
validator.isCuit = require('./isCuit');
validator.isSube = require('./isSube');
validator.isCBU = require('./isCBU');
validator.isCVU = require('./isCVU');
validator.isDNI = require('./isDNI');
validator.isLC = require('./isLC');
validator.isLE = require('./isLE');
validator.isCI = require('./isCI');

module.exports = validator;
