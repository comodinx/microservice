'use strict';

const _ = require('lodash');

const DEFAULT_CARD_SUBE_START_NUMBERS = '6061';

function validate (value) {
    return simpleValidate(value) && verifyDigit(value);
}

function simpleValidate (value) {
    if (!_.isString(value) || _.isEmpty(value)) {
        return false;
    }
    return value.length === 16 && value.substring(0, DEFAULT_CARD_SUBE_START_NUMBERS.length) === DEFAULT_CARD_SUBE_START_NUMBERS;
}

function verifyDigit (value) {
    // String to array
    const digits = value.split('');
    let sum = 0;

    // Delete last item
    digits.pop();

    // Iterate over all digits
    for (let i = 0, l = digits.length; i < l; i++) {
        // ¿Even?
        if (i % 2 === 0) {
            // Digit * 2
            let multiplier = digits[i] * 2;

            // Check overflow
            if (multiplier > 9) {
                multiplier -= 9;
            }

            // Update digit
            digits[i] = multiplier;
        }
        sum += parseInt(digits[i], 10);
    }

    // Module of sum and base10
    const remainder = sum % 10;

    // verificier digit
    let lastdigit = 0;
    if (remainder > 0) {
        lastdigit = 10 - remainder;
    }

    // Verify original number vs the number with the calculated verifier digit
    return (value === (value.substring(0, 15) + lastdigit));
}

module.exports = validate;
