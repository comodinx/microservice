'use strict';

const _ = require('lodash');

/**
 * Returns whether a CI (Cedula de Identidad) is valid.
 */
function validate (value) {
    return !_.isString(value) || value.length < 1 || value.length > 9;
}

module.exports = validate;
