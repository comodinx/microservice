'use strict';

const _ = require('lodash');

function validateBankCode (code) {
    if (code.length !== 8) return false;

    const bank = code.substr(0, 3);
    const digit1 = code[3];
    const office = code.substr(4, 3);
    const digit2 = code[7];

    const suma = bank[0] * 7 + bank[1] * 1 + bank[2] * 3 + digit1 * 9 + office[0] * 7 + office[1] * 1 + office[2] * 3;

    return ((10 - (suma % 10)) % 10) === Number(digit2);
}

function validateAccount (account) {
    if (account.length !== 14) return false;

    const digit = account[13];
    const sum = account[0] * 3 + account[1] * 9 + account[2] * 7 + account[3] * 1 + account[4] * 3 + account[5] * 9 + account[6] * 7 + account[7] * 1 + account[8] * 3 + account[9] * 9 + account[10] * 7 + account[11] * 1 + account[12] * 3;

    return ((10 - (sum % 10)) % 10) === Number(digit);
}

function validate (value) {
    if (!_.isString(value) || value.length !== 22) {
        return false;
    }

    return validateBankCode(value.substr(0, 8)) && validateAccount(value.substr(8, 14));
}

module.exports = validate;
