'use strict';

const _ = require('lodash');
const validator = require('validator');

function validate (value) {
    if (_.isNumber(value)) {
        return true;
    }
    return !_.isEmpty(value) && validator.isNumeric(value);
}

module.exports = validate;
