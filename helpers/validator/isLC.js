'use strict';

const _ = require('lodash');

/**
 * Returns whether a LC (Licencia de Conducir) is valid.
 */
function validate (value) {
    return !_.isString(value) || value.length < 6 || value.length > 7;
}

module.exports = validate;
