'use strict';

const _ = require('lodash');

const directions = ['DESC', 'ASC'];
const defaultMapping = {
    date: 'created_at'
};

module.exports = (query, mapping = {}) => {
    if (!query || !query.order) {
        return;
    }

    mapping = _.merge({}, defaultMapping, mapping);

    return _.map(query.order.split(query.orderSeparator || ','), condition => {
        const [key, direction] = condition.split('-');
        const order = [mapping[key] || key];

        if (direction && directions.includes(direction.toUpperCase())) {
            order.push(direction.toUpperCase());
        }

        return order;
    });
};
