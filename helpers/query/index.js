'use strict';

const query = {};

query.pagination = require('./pagination');
query.filters = require('./filters');
query.without = require('./without');
query.hidden = require('./hidden');
query.order = require('./order');
query.group = require('./group');
query.parse = require('./parse');
query.with = require('./with');
query.term = require('./term');

module.exports = query;
