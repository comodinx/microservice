'use strict';

const _ = require('lodash');
const sequelize = require('sequelize');
const properties = require('./properties');

module.exports = (query, options = {}) => {
    const result = properties(query, 'fields', options);

    if (result && result.fields) {
        result.fields = _.map(result.fields, field => {
            const parts = field.split('-');

            if (parts.length <= 1) {
                return field;
            }
            return [sequelize.fn(parts[0], sequelize.col(parts[1])), parts[2] || (parts[1] !== '*' ? parts[1] : parts[0])];
        });
    }
    return result;
};
