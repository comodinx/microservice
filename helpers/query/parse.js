'use strict';

const _ = require('lodash');
const buildPagination = require('./pagination');
const buildFilters = require('./filters');
const buildRemotes = require('./remotes');
const buildWithout = require('./without');
const buildHidden = require('./hidden');
const buildFields = require('./fields');
const buildOrder = require('./order');
const buildGroup = require('./group');
const buildWith = require('./with');
const buildTerm = require('./term');

const boolParams = ['true', '1'];
const defaultSmall = process.env.MICROSERVICE_QUERY_SMALL || 'true';

function mergeArrays (objValue, srcValue) {
    if (_.isArray(objValue)) {
        return objValue.concat(srcValue);
    }
}

module.exports = (query, options = {}) => {
    const pagination = buildPagination(query, options);
    const withouts = buildWithout(query, options);
    const hiddens = buildHidden(query, options);
    const filters = buildFilters(query, options);
    const remotes = buildRemotes(query, options);
    const fields = buildFields(query, options);
    const orders = buildOrder(query, options);
    const groups = buildGroup(query, options);
    const withs = buildWith(query, options);
    const opts = {
        tiny: boolParams.includes(String((query && query.tiny) || 'true').toLowerCase())
    };

    if (pagination) {
        opts.limit = pagination.limit;
        opts.offset = pagination.offset;
    }
    if (filters) {
        opts.where = filters;
    }
    if (withouts) {
        opts.without = withouts.without;

        if (withouts.relationships) {
            opts.relationships = _.mergeWith(opts.relationships || {}, withouts.relationships, mergeArrays);
        }
    }
    if (hiddens) {
        opts.hidden = hiddens.hidden;

        if (hiddens.relationships) {
            opts.relationships = _.mergeWith(opts.relationships || {}, hiddens.relationships, mergeArrays);
        }
    }
    if (withs) {
        opts.with = withs.with;

        if (withs.relationships) {
            opts.relationships = _.mergeWith(opts.relationships || {}, withs.relationships, mergeArrays);
        }
    }
    if (remotes) {
        if (remotes.with) {
            opts.with = opts.with ? opts.with.concat(remotes.with) : remotes.with;
        }

        if (remotes.remotes) {
            opts.remotes = _.mergeWith(opts.remotes || {}, remotes.remotes, mergeArrays);
        }
    }
    if (fields && fields.fields && fields.fields.length) {
        opts.attributes = fields.fields;
    }
    if (orders && orders.length) {
        opts.order = orders;
    }
    if (groups && groups.length) {
        opts.group = groups;
    }
    if (query && query.small) {
        opts.small = boolParams.includes(String(query.small || defaultSmall).toLowerCase());
    }
    if (query && query.term) {
        opts.term = query.term;
        buildTerm(opts, options);
        delete opts.term;
    }

    return opts;
};
