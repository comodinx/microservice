'use strict';

const _ = require('lodash');

const defaultSeparator = ',';
const defaultConcatenator = '.';

function getDefaultSeparator (type, options) {
    return (_.isObject(options) ? options[`${type}Separator`] : defaultSeparator) || defaultSeparator;
}

function getDefaultConcatenator (type, options) {
    return (_.isObject(options) ? options[`${type}Concatenator`] : defaultConcatenator) || defaultConcatenator;
}

function resolveRelationship (context, type, property, concatenator) {
    const name = property.substring(0, property.indexOf(concatenator));
    const subproperty = property.substring(property.indexOf(concatenator) + 1);

    context.relationships = context.relationships || {};
    context.relationships[name] = context.relationships[name] || {};
    context.relationships[name][type] = context.relationships[name][type] || [];
    context.relationships[name][type].push(subproperty);
    return context;
}

module.exports = (query, type, options = {}) => {
    if (!query || !query[type]) {
        return;
    }

    const concatenator = getDefaultConcatenator(type, options);
    const properties = _.isString(query[type]) ? query[type].split(getDefaultSeparator(type, options)) : query[type];
    const opts = {
        [type]: []
    };

    opts[type] = _.reduce(properties, (carry, property) => {
        if (property.includes(concatenator)) {
            resolveRelationship(opts, type, property, concatenator);
        }
        else {
            carry.push(property);
        }
        return carry;
    }, opts[type]);

    return opts;
};
