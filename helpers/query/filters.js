'use strict';

const _ = require('lodash');
const Sequelize = require('sequelize');
const { Parser, Mappers } = require('@comodinx/query-filters');
const validator = require('../validator');
const Op = Sequelize.Op;

const mapOperator = {
    eq: Op.eq,
    ee: 'ee',
    ne: Op.ne,
    gt: Op.gt,
    ge: Op.gte,
    lt: Op.lt,
    le: Op.lte,
    li: Op.like,
    nl: Op.notLike,
    in: Op.in,
    ni: Op.notIn,
    be: Op.between,
    nb: Op.notBetween,
    is: Op.is,
    no: Op.not
};
const separator = ',';
const key = '[A-Za-z0-9_\.]+'; // eslint-disable-line no-useless-escape
const valueLikeParse = /\*/g; // eslint-disable-line no-useless-escape
const valueLikeFormat = /\%/g; // eslint-disable-line no-useless-escape

// Extends mappers on @comodinx/query-filters
Mappers.Sequelize = mapOperator;

// Json keys
const defaultJsonKeys = ['metadata'];

function buildFilters (query, options = {}) {
    // Save last parser instance.
    const parser = buildFilters.lastParser = createParser(_.merge({}, {
        separator: query.filtersSeparator || separator
    }, options || {}));

    if (!query || !query.filters || _.isEmpty(query.filters)) {
        return;
    }

    const parsed = parser.parse(query.filters);

    if (!parsed) {
        return;
    }

    const jsonKeys = options.jsonKeys || defaultJsonKeys;

    return _.reduce(parsed, (carry, condition, key) => {
        const jsonKey = _.find(jsonKeys, jsonKey => _.startsWith(key, `${jsonKey}.`));

        if (jsonKey) {
            carry[Op.and] = carry[Op.and] || [];
            carry[Op.and].push(Sequelize.where(Sequelize.literal(`${jsonKey}->"$${key.replace(jsonKey, '')}"`), condition));
        }
        else {
            carry[key] = condition;
        }
        return carry;
    }, {});
}

function mapValueParse (value, operator) {
    // exact equal
    if (operator.trim() === 'ee') {
        return value;
    }

    // is or is not
    if (operator.trim() === 'is' || operator.trim() === 'no') {
        if (value === 'null' || value === 'undefined') {
            return null;
        }
        return value;
    }

    if (_.isArray(value)) {
        return _.map(value, value => mapValueParse(value, operator));
    }

    if (validator.isNumber(value)) {
        return Number(value);
    }
    if (validator.isBoolean(value)) {
        return value === 'true' || value === '1';
    }
    if (_.isString(value)) {
        return value.replace(valueLikeParse, '%');
    }
    return value;
}

function mapValueFormat (value, operator) {
    if (_.isArray(value)) {
        return `[${value.map(value => mapValueFormat(value, operator)).join(';')}]`;
    }
    if (_.isObject(value)) {
        return JSON.stringify(value);
    }
    if (_.isString(value)) {
        return value.replace(valueLikeFormat, '*');
    }
    return value;
}

function createParser (options) {
    return new Parser(_.merge({}, {
        mapValueFormat,
        mapValueParse,
        mapOperator,
        separator,
        key
    }, options || {}));
}

buildFilters.createParser = createParser;

module.exports = buildFilters;
