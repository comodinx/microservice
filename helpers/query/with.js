'use strict';

const properties = require('./properties');

module.exports = (query, options = {}) => properties(query, 'with', options);
