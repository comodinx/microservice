'use strict';

const _ = require('lodash');

// eslint-disable-next-line no-useless-escape
const valueLikeFormat = /[\(\)]/g;
const firstLevelSeparator = '),';
const defaultConcatenator = '.';

function buildRemotes (query, options) {
    if (!query || !query.remotes) {
        return;
    }

    const opts = {
        with: [],
        remotes: {}
    };
    const properties = query.remotes.split(firstLevelSeparator);

    _.each(properties, property => {
        const [relationsOperation, params] = property.split(valueLikeFormat);
        const relations = relationsOperation.split(defaultConcatenator);
        const operation = relations.pop();
        const firstRelation = relations.shift();

        if (relations.length > 0) {
            opts.remotes[firstRelation] = opts.remotes[firstRelation] || {};

            const remoteRelation = opts.remotes[firstRelation];
            const condition = `${relations.join(defaultConcatenator)}${defaultConcatenator}${operation}(${params})`;

            remoteRelation.remotes = remoteRelation.remotes ? `${remoteRelation.remotes},${condition}` : condition;
        }
        else {
            opts.remotes[firstRelation] = opts.remotes[firstRelation] || {};
            opts.remotes[firstRelation][operation] = params;
        }

        // extract "withs" of remotes
        const withs = _.transform([firstRelation, ...relations], (result, element) => {
            const elemResult = result.length ? `${_.last(result)}${defaultConcatenator}${element}` : element;
            result.push(elemResult);
        }, []);

        opts.with = _.uniq([...opts.with, ...withs]);
    });

    return opts;
}

module.exports = buildRemotes;
