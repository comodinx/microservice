'use strict';

const _ = require('lodash');
const { Op } = require('sequelize');
const validator = require('../validator');

const defaultMapping = {
    numeric: ['id']
};

const regexpWild = /\*/g;
const empty = '';
const wild = '%';

function cleanTerm (term, replacement = wild) {
    return `${term.replace(regexpWild, replacement)}`;
}

function prepareField (query, field, term) {
    query.where = query.where || {};
    query.where[field] = query.where[field] || {};
    query.where[field][Op.like] = cleanTerm(term);
    return query.where;
}

module.exports = (query, mapping = {}) => {
    if (!query || !query.term || !_.trim(query.term)) {
        return;
    }

    const term = _.trim(query.term);
    mapping = _.merge({}, defaultMapping, mapping);

    if (validator.isNumber(cleanTerm(term, empty)) && mapping.numeric && mapping.numeric.length) {
        _.each(mapping.numeric, field => prepareField(query, field, term));
    }

    if (validator.isAlpha(cleanTerm(term, empty)) && mapping.alpha && mapping.alpha.length) {
        _.each(mapping.alpha, field => prepareField(query, field, term));
    }

    if (validator.isAlphanumeric(cleanTerm(term, empty)) && mapping.alphanumeric && mapping.alphanumeric.length) {
        _.each(mapping.alphanumeric, field => prepareField(query, field, term));
    }
    return query;
};
