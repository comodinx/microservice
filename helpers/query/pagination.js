'use strict';

const _ = require('lodash');

const defaultPageSize = 0;

function getDefaultPageSize (options) {
    if (_.isNumber(options)) {
        return options;
    }
    return (_.isObject(options) ? options.pageSize : defaultPageSize) || defaultPageSize;
}

module.exports = (query, options) => {
    if (!query) {
        return;
    }

    const pageSize = Number(query.page_size || query.pageSize || getDefaultPageSize(options));
    const page = Number(query.page || 1) - 1;

    if (!pageSize) {
        return;
    }

    return {
        limit: pageSize,
        offset: page * pageSize
    };
};
