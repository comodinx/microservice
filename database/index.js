'use strict';

const DB_ENABLED = process.env.MICROSERVICE_DB_ENABLED || process.env.DB_ENABLED;

// Prevent load unecesary dependecies
if (!['0', 'false'].includes(String(DB_ENABLED).toLowerCase())) {
    module.exports = require('./initialize');
}
