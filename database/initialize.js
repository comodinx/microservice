'use strict';

const _ = require('lodash');
const P = require('bluebird');
const Model = require('./model');
const config = require('../config');
const logger = require('../helpers/logger');
const { Op, QueryTypes, fn, col, literal } = require('sequelize');
const { Database } = require('@comodinx/sequelize');

const asyncDatabaseInitialze = !['0', 'false'].includes(String(process.env.MICROSERVICE_DB_ASYNC_DISCOVER).toLowerCase());

function initialize () {
    const database = new Database(config('database'));
    let models = {};

    // We handle the extension of models, preventing desynchronization with autodiscover.
    Object.defineProperty(database, 'models', {
        configurable: true,
        enumerable: true,
        get: function () {
            return models;
        },
        set: function (dataModels) {
            _.each(models, (model, name) => {
                dataModels[name] = model;
            });
            models = dataModels;
        }
    });

    // Short cuts for use sequelize functionality without require on your microservices
    database.Op = Op;
    database.QueryTypes = QueryTypes;
    database.literal = literal;
    database.Model = Model;
    database.fn = fn;
    database.col = col;

    if (asyncDatabaseInitialze) {
        // We discover the models of the database automatically and ASYNCHRONOUSLY.
        setTimeout(() => {
            return database
                .discover()
                .then(() => {
                    logger.lap(`Ready-to-use database. ${database.tables ? _.size(database.tables) : 0} models were found.`);
                    return P.resolve(database);
                })
                .catch(P.reject);
        }, 1);

        return database;
    }

    // Discover the models of the database automatically and SYNCHRONOUSLY.
    return database
        .discover()
        .then(() => {
            logger.lap(`Ready-to-use database. ${database.tables ? _.size(database.tables) : 0} models were found.`);
            return P.resolve(database);
        });
}

module.exports = initialize();
