'use strict';

const _ = require('lodash');
const P = require('bluebird');
const pluralize = require('pluralize');
const { Model } = require('@comodinx/sequelize');
const service = require('../helpers/service');
const logger = require('../helpers/logger');
const config = require('../config');

class BaseModel extends Model {
    constructor (options) {
        super(_.merge(options || {}, {
            readOptions: config('database.readOptions', {}),
            writeOptions: config('database.writeOptions', {})
        }));
    }

    populateRemote (model, relationshipName, options, uri = null, serviceName = null) {
        const singularRelationshipName = relationshipName;
        const pluralRelationshipName = pluralize(singularRelationshipName);
        const field = `id_${singularRelationshipName}`;

        // Without does not remove currency properties
        if (this.includeInWithout(options, singularRelationshipName)) {
            return P.resolve(model);
        }

        // Check With option
        if (!this.includeInWith(options, singularRelationshipName)) {
            return P.resolve(model);
        }

        // Hidden remove currency properties
        if (this.includeInHidden(options, singularRelationshipName)) {
            delete model[field];
            delete model[singularRelationshipName];
            return P.resolve(model);
        }

        const id = model[field];

        // Prevent unnecesary call service.
        if (!id) {
            return P.resolve(model);
        }

        const serviceAliases = this.options.serviceAliases || {};
        const opts = this.prepareRemoteOptions(options, uri, pluralRelationshipName, singularRelationshipName, id);

        return (options.service || service)[serviceName || serviceAliases[pluralRelationshipName] || pluralRelationshipName]
            .get(opts)
            .then(relationship => {
                if (relationship) {
                    delete model[field];
                }
                model[singularRelationshipName] = relationship;
                return model;
            })
            .catch(error => {
                logger.warn(`Error on populate model relationship [${singularRelationshipName}]. ${error}`);
                return model;
            });
    }

    populateRemotes (model, relationshipName, options, uri = null, serviceName = null) {
        const pluralRelationshipName = relationshipName;
        const singularRelationshipName = pluralize.singular(pluralRelationshipName);
        const field = `id_${singularRelationshipName}`;

        // Without does not remove currency properties
        if (this.includeInWithout(options, pluralRelationshipName)) {
            return P.resolve(model);
        }

        // Check With option
        if (!this.includeInWith(options, pluralRelationshipName)) {
            return P.resolve(model);
        }

        // Hidden remove currency properties
        if (this.includeInHidden(options, pluralRelationshipName)) {
            delete model[pluralRelationshipName];
            return P.resolve(model);
        }

        const ids = _.map(model[pluralRelationshipName], field);

        // Prevent unnecesary call service.
        if (!ids || !ids.length) {
            return P.resolve(model);
        }

        const serviceAliases = this.options.serviceAliases || {};
        const opts = this.prepareRemotesOptions(options, uri, pluralRelationshipName, singularRelationshipName, ids);

        return (options.service || service)[serviceName || serviceAliases[pluralRelationshipName] || pluralRelationshipName]
            .get(opts)
            .then(relationships => {
                model[pluralRelationshipName] = relationships;
                return model;
            })
            .catch(error => {
                logger.warn(`Error on populate model relationships [${pluralRelationshipName}]. ${error}`);
                return model;
            });
    }

    includeInWithout (options, ...keys) {
        return options.without && options.without.some(k => keys.includes(k));
    }

    includeInWith (options, ...keys) {
        return !options.tiny || (options.with && options.with.some(k => keys.includes(k)));
    }

    includeInHidden (options, ...keys) {
        return options.hidden && options.hidden.some(k => keys.includes(k));
    }

    prepareRemoteOptions (options, uri, model, key, id) {
        return {
            uri: `${uri || `/${model}`}/${id}`,
            qs: this.prepareQueryOptions(options, key)
        };
    }

    prepareRemotesOptions (options, uri, model, key, ids) {
        return {
            uri: uri || `/${model}`,
            qs: _.merge({}, this.prepareQueryOptions(options, model), {
                // Optimize filter by ID when only has one item in ids.
                filters: `id ${ids.length === 1 ? `eq ${ids[0]}` : `in [${ids.join(';')}]`}`
            })
        };
    }

    prepareQueryOptions (options, key) {
        const query = {};

        _.merge(query, this.prepareQueryByRelationship(query, options, key));
        _.merge(query, this.prepareQueryByFilters(query, options, key));

        _.each(['hidden', 'with', 'without'], prop => {
            if (_.isArray(query[prop])) {
                query[prop] = query[prop].join(',');
            }
        });
        return query;
    }

    prepareQueryByRelationship (query, options, key) {
        const relationship = (options.relationships && options.relationships[key]) || null;
        query = query || {};

        if (!relationship) {
            return query;
        }

        _.each(relationship, (condition, name) => {
            switch (name) {
                case 'hidden':
                case 'without':
                case 'with':
                    query[name] = _.isString(relationship[name]) ? relationship[name] : relationship[name].join(',');
                    break;

                case 'small':
                case 'tiny':
                    query[name] = _.isString(relationship[name]) ? ['true', '0'].includes(relationship[name].toLowerCase()) : relationship[name];
                    break;

                default:
                    query[name] = relationship[name];
                    break;
            }
        });
        return query;
    }

    prepareQueryByFilters (query, options, key) {
        const relationship = (options.relationships && options.relationships[key]) || null;
        query = query || {};

        if (relationship) {
            // "Without" does not remove relationship properties
            if (relationship.without && relationship.without) {
                query.without = relationship.without.join(',');
            }
            // "Hidden" remove relationship properties
            if (relationship.hidden && relationship.hidden) {
                query.hidden = relationship.hidden.join(',');
            }
            // "With" include relationship properties
            if (relationship.with && relationship.with) {
                query.with = relationship.with.join(',');
            }
            // "Small"  relationship properties
            if (relationship.small) {
                query.small = true;
            }
            // "Tiny" relationship properties
            if (relationship.tiny) {
                query.tiny = true;
            }
        }
        else {
            const modelOptionRegExp = new RegExp(`${key}.`, 'i');
            const getRelationshipOptions = (options) => {
                const replaced = _.map(options, (element) => {
                    if (modelOptionRegExp.test(element)) { return element.replace(modelOptionRegExp, ''); }
                });
                return _.compact(replaced);
            };

            // "With" included in options.with='account.*'
            if (options.with) {
                const withOpts = getRelationshipOptions(options.with);
                if (withOpts.length) query.with = withOpts;
            }
            // "Without" included in options.without='account.*'
            if (options.without) {
                const withoutOpts = getRelationshipOptions(options.without);
                if (withoutOpts.length) query.without = withoutOpts;
            }
            // "Hidden" included in options.hidden='account.*'
            if (options.hidden) {
                const hiddenOpts = getRelationshipOptions(options.hidden);
                if (hiddenOpts.length) query.hidden = hiddenOpts;
            }
        }
        return query;
    }

    getPickOptionsForPopulateRelationship () {
        return super.getPickOptionsForPopulateRelationship().concat(['extra']);
    }
}

module.exports = BaseModel;
