'use strict';

const _ = require('lodash');
const P = require('bluebird');
const mock = require('mock-require');
const MockExpressRequest = require('mock-express-request');
const MockExpressResponse = require('mock-express-response');
const errors = require('../errors');

mock('../database', {
    models: {
        MyModel: {
            getById: id => P.resolve(id === 1 ? null : { id }),
            update: id => P.resolve(id === 1 ? null : { id })
        }
    }
});

const statuses = Object.keys(errors).map(k => errors[k].code).filter(code => !!code);
const DEFAULT_ERROR_MESSAGE = 'Internal Server Error';
const DEFAULT_ERROR_STATUS = errors.InternalServerError.code;
const next = (req, res, error) => {
    if (error instanceof errors.CustomError) {
        if (!error.code || !_.isNumber(error.code)) {
            error.code = DEFAULT_ERROR_STATUS;
        }
        return res.status(error.code).send(error.toJson());
    }

    const message = (error && error.message) || DEFAULT_ERROR_MESSAGE;
    let status = Number((error && (error.status || error.statusCode || error.responseCode || error.code)) || DEFAULT_ERROR_STATUS);

    // Check if status is a valid http status code
    if (!statuses.includes(status)) {
        status = DEFAULT_ERROR_STATUS;
    }

    return res.status(status).send((new errors.CustomError(message, status)).toJson());
};

const RouteGetById = require('../server/routes/getById');
const RouteDestroy = require('../server/routes/destroy');

describe('server', () => {
    describe('Routes', () => {
        describe('getById', () => {
            it('should has route instance', done => {
                const route = new RouteGetById('MyModel');

                expect(route).to.not.be.an('undefined');
                done();
            });

            it('should return 200 when id is correct', done => {
                const route = new RouteGetById('MyModel');
                const req = new MockExpressRequest({ params: { id: 2 } });
                const res = new MockExpressResponse({ request: req });

                route.handle(req, res, next.bind(next, req, res))
                    .then(() => {
                        expect(res.statusCode).to.be.an('number').equal(errors.Ok.code);
                        done();
                    })
                    .catch(done);
            });

            it('should return 200 when has an integer "id" on params', done => {
                const route = new RouteGetById('MyModel');
                const req = new MockExpressRequest({ params: { id: 2 } });
                const res = new MockExpressResponse({ request: req });

                route.handle(req, res, next.bind(next, req, res))
                    .then(() => {
                        expect(res.statusCode).to.be.an('number').equal(errors.Ok.code);
                        done();
                    })
                    .catch(done);
            });

            it('should return 200 when has a custom param type "string" for "id" on params', done => {
                const route = new RouteGetById('MyModel', { validatorIdType: 'isAlphanumeric' });
                const req = new MockExpressRequest({ params: { id: 'AbCd1234' } });
                const res = new MockExpressResponse({ request: req });

                route.handle(req, res, next.bind(next, req, res))
                    .then(() => {
                        expect(res.statusCode).to.be.an('number').equal(errors.Ok.code);
                        done();
                    })
                    .catch(done);
            });

            it('should return 200 when has a custom param name "id_my_model" on params', done => {
                const route = new RouteGetById('MyModel', { paramNameId: 'id_my_model' });
                const req = new MockExpressRequest({ params: { id_my_model: 2 } });
                const res = new MockExpressResponse({ request: req });

                route.handle(req, res, next.bind(next, req, res))
                    .then(() => {
                        expect(res.statusCode).to.be.an('number').equal(errors.Ok.code);
                        done();
                    })
                    .catch(done);
            });

            it('should throw error 400 when not has "id" on params', done => {
                const route = new RouteGetById('MyModel');
                const req = new MockExpressRequest();
                const res = new MockExpressResponse({ request: req });

                route.handle(req, res, next.bind(next, req, res))
                    .then(() => {
                        expect(res.statusCode).to.be.an('number').equal(errors.BadRequest.code);
                        done();
                    })
                    .catch(done);
            });

            it('should throw error 400 when not has an integer "id" on params', done => {
                const route = new RouteGetById('MyModel');
                const req = new MockExpressRequest({ params: { id: 'AbCd1234' } });
                const res = new MockExpressResponse({ request: req });

                route.handle(req, res, next.bind(next, req, res))
                    .then(() => {
                        expect(res.statusCode).to.be.an('number').equal(errors.BadRequest.code);
                        done();
                    })
                    .catch(done);
            });

            it('should throw error 400 when not has a custom param type "string" for "id" on params', done => {
                const route = new RouteGetById('MyModel', { validatorIdType: 'isAlphanumeric' });
                const req = new MockExpressRequest({ params: { id: '$1' } });
                const res = new MockExpressResponse({ request: req });

                route.handle(req, res, next.bind(next, req, res))
                    .then(() => {
                        expect(res.statusCode).to.be.an('number').equal(errors.BadRequest.code);
                        done();
                    })
                    .catch(done);
            });

            it('should throw error 400 when not has a custom param name "id_my_model" on params', done => {
                const route = new RouteGetById('MyModel', { paramNameId: 'id_my_model' });
                const req = new MockExpressRequest({ params: { id: 1 } });
                const res = new MockExpressResponse({ request: req });

                route.handle(req, res, next.bind(next, req, res))
                    .then(() => {
                        expect(res.statusCode).to.be.an('number').equal(errors.BadRequest.code);
                        done();
                    })
                    .catch(done);
            });

            it('should throw error 404 when not has model for id', done => {
                const route = new RouteGetById('MyModel');
                const req = new MockExpressRequest({ params: { id: 1 } });
                const res = new MockExpressResponse({ request: req });

                route.handle(req, res, next.bind(next, req, res))
                    .then(() => {
                        expect(res.statusCode).to.be.an('number').equal(errors.NotFound.code);
                        done();
                    })
                    .catch(done);
            });

            after(() => mock.stopAll());
        });

        describe('destroy', () => {
            it('should has route instance', done => {
                const route = new RouteDestroy('MyModel');

                expect(route).to.not.be.an('undefined');
                done();
            });

            it('should return 204 when id is correct', done => {
                const route = new RouteDestroy('MyModel');
                const req = new MockExpressRequest({ params: { id: 2 } });
                const res = new MockExpressResponse({ request: req });

                route.handle(req, res, next.bind(next, req, res))
                    .then(() => {
                        expect(res.statusCode).to.be.an('number').equal(errors.NoContent.code);
                        done();
                    })
                    .catch(done);
            });

            it('should return 204 when has an integer "id" on params', done => {
                const route = new RouteDestroy('MyModel');
                const req = new MockExpressRequest({ params: { id: 2 } });
                const res = new MockExpressResponse({ request: req });

                route.handle(req, res, next.bind(next, req, res))
                    .then(() => {
                        expect(res.statusCode).to.be.an('number').equal(errors.NoContent.code);
                        done();
                    })
                    .catch(done);
            });

            it('should return 204 when has a custom param type "string" for "id" on params', done => {
                const route = new RouteDestroy('MyModel', { validatorIdType: 'isAlphanumeric' });
                const req = new MockExpressRequest({ params: { id: 'AbCd1234' } });
                const res = new MockExpressResponse({ request: req });

                route.handle(req, res, next.bind(next, req, res))
                    .then(() => {
                        expect(res.statusCode).to.be.an('number').equal(errors.NoContent.code);
                        done();
                    })
                    .catch(done);
            });

            it('should return 204 when has a custom param name "id_my_model" on params', done => {
                const route = new RouteDestroy('MyModel', { paramNameId: 'id_my_model' });
                const req = new MockExpressRequest({ params: { id_my_model: 2 } });
                const res = new MockExpressResponse({ request: req });

                route.handle(req, res, next.bind(next, req, res))
                    .then(() => {
                        expect(res.statusCode).to.be.an('number').equal(errors.NoContent.code);
                        done();
                    })
                    .catch(done);
            });

            it('should throw error 400 when not has "id" on params', done => {
                const route = new RouteDestroy('MyModel');
                const req = new MockExpressRequest();
                const res = new MockExpressResponse({ request: req });

                route.handle(req, res, next.bind(next, req, res))
                    .then(() => {
                        expect(res.statusCode).to.be.an('number').equal(errors.BadRequest.code);
                        done();
                    })
                    .catch(done);
            });

            it('should throw error 400 when not has an integer "id" on params', done => {
                const route = new RouteDestroy('MyModel');
                const req = new MockExpressRequest({ params: { id: 'AbCd1234' } });
                const res = new MockExpressResponse({ request: req });

                route.handle(req, res, next.bind(next, req, res))
                    .then(() => {
                        expect(res.statusCode).to.be.an('number').equal(errors.BadRequest.code);
                        done();
                    })
                    .catch(done);
            });

            it('should throw error 400 when not has a custom param type "string" for "id" on params', done => {
                const route = new RouteDestroy('MyModel', { validatorIdType: 'isAlphanumeric' });
                const req = new MockExpressRequest({ params: { id: '$1' } });
                const res = new MockExpressResponse({ request: req });

                route.handle(req, res, next.bind(next, req, res))
                    .then(() => {
                        expect(res.statusCode).to.be.an('number').equal(errors.BadRequest.code);
                        done();
                    })
                    .catch(done);
            });

            it('should throw error 400 when not has a custom param name "id_my_model" on params', done => {
                const route = new RouteDestroy('MyModel', { paramNameId: 'id_my_model' });
                const req = new MockExpressRequest({ params: { id: 1 } });
                const res = new MockExpressResponse({ request: req });

                route.handle(req, res, next.bind(next, req, res))
                    .then(() => {
                        expect(res.statusCode).to.be.an('number').equal(errors.BadRequest.code);
                        done();
                    })
                    .catch(done);
            });

            it('should throw error 404 when not has model for id', done => {
                const route = new RouteDestroy('MyModel');
                const req = new MockExpressRequest({ params: { id: 1 } });
                const res = new MockExpressResponse({ request: req });

                route.handle(req, res, next.bind(next, req, res))
                    .then(() => {
                        expect(res.statusCode).to.be.an('number').equal(errors.NotFound.code);
                        done();
                    })
                    .catch(done);
            });

            after(() => mock.stopAll());
        });
    });
});
