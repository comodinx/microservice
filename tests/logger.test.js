'use strict';

const logger = require('../helpers/logger');

logger.logger.silent = true;

describe('logger', () => {
    describe('Commons', () => {
        beforeEach(function () {
            this.sinon.stub(logger, 'error');
            this.sinon.stub(logger, 'warn');
            this.sinon.stub(logger, 'info');
        });

        it('should log with error level', done => {
            logger.error('Log level error');
            expect(logger.error.calledOnce).to.be.equal(true);
            expect(logger.error.calledWith('Log level error')).to.be.equal(true);
            done();
        });

        it('should log with warn level', done => {
            logger.warn('Log level warn');
            expect(logger.warn.calledOnce).to.be.equal(true);
            expect(logger.warn.calledWith('Log level warn')).to.be.equal(true);
            done();
        });

        it('should log with info level', done => {
            logger.info('Log level info');
            expect(logger.info.calledOnce).to.be.equal(true);
            expect(logger.info.calledWith('Log level info')).to.be.equal(true);
            done();
        });
    });

    describe('Customs', () => {
        beforeEach(function () {
            this.sinon.stub(logger, 'title');
            this.sinon.stub(logger, 'success');
            this.sinon.stub(logger, 'arrow');
            this.sinon.stub(logger, 'step');
            this.sinon.stub(logger, 'lap');
        });

        it('should log with title format', done => {
            logger.title('Log title');
            expect(logger.title.calledOnce).to.be.equal(true);
            expect(logger.title.calledWith('Log title')).to.be.equal(true);
            done();
        });

        it('should log with success format', done => {
            logger.success('Log success');
            expect(logger.success.calledOnce).to.be.equal(true);
            expect(logger.success.calledWith('Log success')).to.be.equal(true);
            done();
        });

        it('should log with arrow format', done => {
            logger.arrow('Log arrow');
            expect(logger.arrow.calledOnce).to.be.equal(true);
            expect(logger.arrow.calledWith('Log arrow')).to.be.equal(true);
            done();
        });

        it('should log with step format', done => {
            logger.step('Log step');
            expect(logger.step.calledOnce).to.be.equal(true);
            expect(logger.step.calledWith('Log step')).to.be.equal(true);
            done();
        });

        it('should log with lap format', done => {
            logger.lap('Log lap');
            expect(logger.lap.calledOnce).to.be.equal(true);
            expect(logger.lap.calledWith('Log lap')).to.be.equal(true);
            done();
        });
    });
});
