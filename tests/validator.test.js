'use strict';

const validator = require('../helpers/validator');

describe('validator', () => {
    describe('Customs', () => {
        it('should validate cuits', done => {
            expect(validator.isCuit()).to.be.equal(false);
            expect(validator.isCuit(null)).to.be.equal(false);
            expect(validator.isCuit('307164')).to.be.equal(false);
            expect(validator.isCuit(30716465221)).to.be.equal(false);
            expect(validator.isCuit('30716465221')).to.be.equal(true);
            done();
        });

        it('should validate sube card number', done => {
            expect(validator.isSube()).to.be.equal(false);
            expect(validator.isSube(null)).to.be.equal(false);
            expect(validator.isSube('606126834663')).to.be.equal(false);
            expect(validator.isSube(6061268346636331)).to.be.equal(false);
            expect(validator.isSube('6061268346636331')).to.be.equal(true);
            done();
        });

        it('should validate integer', done => {
            expect(validator.isInteger()).to.be.equal(false);
            expect(validator.isInteger(null)).to.be.equal(false);
            expect(validator.isInteger('a')).to.be.equal(false);
            expect(validator.isInteger('6')).to.be.equal(true);
            expect(validator.isInteger('6.1')).to.be.equal(false);
            expect(validator.isInteger('-6')).to.be.equal(true);
            expect(validator.isInteger(6)).to.be.equal(true);
            expect(validator.isInteger(6.1)).to.be.equal(false);
            expect(validator.isInteger(-6)).to.be.equal(true);
            done();
        });

        it('should validate number', done => {
            expect(validator.isNumber()).to.be.equal(false);
            expect(validator.isNumber(null)).to.be.equal(false);
            expect(validator.isNumber('a')).to.be.equal(false);
            expect(validator.isNumber('6')).to.be.equal(true);
            expect(validator.isNumber('6.1')).to.be.equal(true);
            expect(validator.isNumber('-6')).to.be.equal(true);
            expect(validator.isNumber(6)).to.be.equal(true);
            expect(validator.isNumber(6.1)).to.be.equal(true);
            expect(validator.isNumber(-6)).to.be.equal(true);
            done();
        });

        it('should validate CBU', done => {
            expect(validator.isCBU()).to.be.equal(false);
            expect(validator.isCBU(null)).to.be.equal(false);
            expect(validator.isCBU('307164')).to.be.equal(false);
            expect(validator.isCBU(30716465221)).to.be.equal(false);
            expect(validator.isCBU('2810590940090418135201')).to.be.equal(false);
            expect(validator.isCBU('2850590940090418135211')).to.be.equal(false);
            expect(validator.isCBU('28505909400904181352012')).to.be.equal(false);
            expect(validator.isCBU('2850590940090418135201')).to.be.equal(true);
            done();
        });

        it('should validate CVU', done => {
            expect(validator.isCVU()).to.be.equal(false);
            expect(validator.isCVU(null)).to.be.equal(false);
            expect(validator.isCVU('307164')).to.be.equal(false);
            expect(validator.isCVU(30716465221)).to.be.equal(false);
            expect(validator.isCBU('2810590940090418135201')).to.be.equal(false);
            expect(validator.isCBU('2850590940090418135211')).to.be.equal(false);
            expect(validator.isCBU('28505909400904181352012')).to.be.equal(false);
            expect(validator.isCVU('2850590940090418135201')).to.be.equal(false);
            expect(validator.isCVU('0000249800000000100250')).to.be.equal(false);
            expect(validator.isCVU('0000248800000000100250')).to.be.equal(true);
            done();
        });
    });
});
