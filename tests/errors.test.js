'use strict';

const errors = require('../errors');

describe('errors', () => {
    Object.keys(errors).forEach(errorName => {
        const ErrorClass = errors[errorName];

        if (!ErrorClass.code) {
            return;
        }

        describe(errorName, () => {
            it(`should return code/status ${ErrorClass.code}`, done => {
                const result = new ErrorClass(errorName);

                expect(result).to.have.property('code').to.be.a('number').equal(ErrorClass.code);
                expect(result).to.have.property('message').to.be.a('string').equal(errorName);
                done();
            });
        });
    });

    describe('JsonErrorResponse', () => {
        it(`should return code/status ${errors.InternalServerError.code}`, done => {
            const data = { a: 'b', c: 1 };
            const result = new errors.JsonErrorResponse(data);

            expect(result).to.have.property('code').to.be.a('number').equal(errors.InternalServerError.code);
            expect(result).to.have.property('message').to.be.a('string').equal('Json Error Response');
            expect(result.toJson()).to.have.property('a').to.be.a('string').equal(data.a);
            done();
        });
    });

    describe('CustomError', () => {
        it(`should return code/status ${errors.InternalServerError.code}`, done => {
            const result = new errors.CustomError('Custom Error');

            expect(result).to.have.property('code').to.be.a('number').equal(errors.InternalServerError.code);
            expect(result).to.have.property('message').to.be.a('string').equal('Custom Error');
            done();
        });
    });

    describe('from', () => {
        beforeEach(function () {
            this.sinon.stub(errors, 'log');
        });

        after(function () {
            this.sinon.restore();
        });

        it('should return the correct error when error have a property error', done => {
            const exception = {
                error: {
                    error: 'Custom Test Error with error',
                    code: 500,
                    extra: {
                        hello: 'world'
                    }
                }
            };

            const result = errors.from(exception);
            expect(result).to.have.property('message').to.be.a('string').equal('Custom Test Error with error');
            expect(result).to.have.property('code').to.be.a('number').equal(errors.InternalServerError.code);
            expect(result).to.have.property('extra').to.be.a('object');
            expect(result.extra).to.have.property('hello').to.be.a('string').equal('world');
            expect(errors.log).to.be.calledOnce; // eslint-disable-line no-unused-expressions

            const json = result.toJson();
            expect(json).to.have.property('extra').to.have.property('hello').to.be.a('string').equal('world');
            done();
        });

        it('should return the correct error when error have a property response', done => {
            const exception = {
                response: {
                    data: {
                        error: 'Custom Test Error with response',
                        code: 404,
                        extra: {
                            hello: 'world'
                        }
                    }
                }
            };

            const result = errors.from(exception);
            expect(result).to.have.property('message').to.be.a('string').equal('Custom Test Error with response');
            expect(result).to.have.property('code').to.be.a('number').equal(errors.NotFound.code);
            expect(result).to.have.property('extra').to.be.a('object');
            expect(result.extra).to.have.property('hello').to.be.a('string').equal('world');
            expect(errors.log).to.be.calledOnce; // eslint-disable-line no-unused-expressions

            const json = result.toJson();
            expect(json).to.have.property('extra').to.have.property('hello').to.be.a('string').equal('world');
            done();
        });

        it('should return the correct error when error have a property name', done => {
            const exception = {
                name: 'BadRequest',
                message: 'Custom Test Error with name',
                code: 400,
                extra: {
                    hello: 'world'
                }
            };

            const result = errors.from(exception);
            expect(result).to.have.property('message').to.be.a('string').equal('Custom Test Error with name');
            expect(result).to.have.property('code').to.be.a('number').equal(errors.BadRequest.code);
            expect(result).to.have.property('extra').to.be.a('object');
            expect(result.extra).to.have.property('hello').to.be.a('string').equal('world');
            expect(errors.log).to.be.calledOnce; // eslint-disable-line no-unused-expressions

            const json = result.toJson();
            expect(json).to.have.property('extra').to.have.property('hello').to.be.a('string').equal('world');
            done();
        });

        it('should return the correct error when error is instance of CustomError', done => {
            const exception = new errors.Unauthorized('User is unauthorized for this action.', {
                hello: 'world'
            });

            const result = errors.from(exception);
            expect(result).to.have.property('message').to.be.a('string').equal('User is unauthorized for this action.');
            expect(result).to.have.property('code').to.be.a('number').equal(errors.Unauthorized.code);
            expect(result).to.have.property('extra').to.be.a('object');
            expect(result.extra).to.have.property('hello').to.be.a('string').equal('world');
            expect(errors.log).to.be.calledOnce; // eslint-disable-line no-unused-expressions

            const json = result.toJson();
            expect(json).to.have.property('extra').to.have.property('hello').to.be.a('string').equal('world');
            done();
        });

        it('should return the correct error when error is instance of Error', done => {
            const exception = new Error('Native Error');

            const result = errors.from(exception);
            expect(result).to.have.property('message').to.be.a('string').equal('Native Error');
            expect(result).to.have.property('code').to.be.a('number').equal(errors.InternalServerError.code);
            expect(errors.log).to.be.calledOnce; // eslint-disable-line no-unused-expressions
            done();
        });

        it('should return the correct error when error have a not mapped error code', done => {
            const exception = {
                response: {
                    data: {
                        error: 'Custom Test Error with 534 code',
                        code: 534,
                        extra: {
                            hello: 'world'
                        }
                    }
                }
            };

            const result = errors.from(exception);
            expect(result).to.have.property('message').to.be.a('string').equal('Custom Test Error with 534 code');
            expect(result).to.have.property('code').to.be.a('number').equal(534);
            expect(result).to.have.property('extra').to.be.a('object');
            expect(result.extra).to.have.property('hello').to.be.a('string').equal('world');
            expect(errors.log).to.be.calledOnce; // eslint-disable-line no-unused-expressions

            const json = result.toJson();
            expect(json).to.have.property('extra').to.have.property('hello').to.be.a('string').equal('world');
            done();
        });
    });
});
