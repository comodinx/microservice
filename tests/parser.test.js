'use strict';

const parser = require('../helpers/parser');

const bogusJsonString = '{"datosTicket":{"Fecha":"11/11/2020","Hora":"02:32","Id":808100184,"IdProveedor":VACQBQMI59                    }}';
const rightJsonString = '{"datosTicket":{"Fecha":"11/11/2020","Hora":"02:32","Id":808100184,"IdProveedor":"VACQBQMI59"}}';
const wrongJsonString = '{"datosTicket":{"Fecha":"11/11/2020","Hora":"02:32","Id":808100184,"IdProveedor":"VACQBQMI59"';
const rightParsedJson = { datosTicket: { Fecha: '11/11/2020', Hora: '02:32', Id: 808100184, IdProveedor: 'VACQBQMI59' } };

describe('parser', () => {
    describe('Json By Eval', () => {
        it('should parse bogus json string', done => {
            expect(parser.jsonByEval(bogusJsonString)).to.be.deep.equal(rightParsedJson);
            done();
        });

        it('should parse json string', done => {
            expect(parser.jsonByEval(rightJsonString)).to.be.deep.equal(rightParsedJson);
            done();
        });

        it('should not parse wrong json string', done => {
            expect(parser.jsonByEval(wrongJsonString)).to.be.equal(undefined);
            done();
        });
    });
});
