'use strict';

const _ = require('lodash');
const config = require('../config');

describe('service', () => {
    let Service;

    before(() => {
        Service = require('../helpers/service');
    });

    describe('Instances', () => {
        _.each(config('service'), service => {
            it(`should has ${service.name} instance`, done => {
                expect(Service[service.name]).to.not.be.an('undefined');
                done();
            });
        });
    });
});
