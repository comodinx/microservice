'use strict';

const { Op } = require('sequelize');
const query = require('../helpers/query');

describe('query', () => {
    describe('Pagination', () => {
        it('should return object with limit and offset', done => {
            const qs = {
                pageSize: 10,
                page: 1
            };

            const result = query.pagination(qs);

            expect(result).to.be.a('object');
            expect(result).to.have.property('limit').to.be.a('number').equal(qs.pageSize);
            expect(result).to.have.property('offset').to.be.a('number').equal(0);
            done();
        });

        it('should return object with correct offset', done => {
            const qs = {
                pageSize: 10,
                page: 2
            };

            const result = query.pagination(qs);

            expect(result).to.be.a('object');
            expect(result).to.have.property('limit').to.be.a('number').equal(qs.pageSize);
            expect(result).to.have.property('offset').to.be.a('number').equal(10);
            done();
        });

        it('should return undefined when query string does not have pageSize and page', done => {
            const qs = {};

            const result = query.pagination(qs);

            expect(result).to.be.a('undefined');
            done();
        });
    });

    describe('Filters', () => {
        it('should return object with active and description', done => {
            const qs = {
                filters: 'active eq 1,description li %casa'
            };

            const result = query.filters(qs);

            expect(result).to.be.a('object');
            expect(result).to.have.property('active').to.be.a('object');
            expect(result).to.have.property('description').to.be.a('object');
            done();
        });

        it('should return object with active and description with correct operators', done => {
            const qs = {
                filters: 'active ee 1,description li %casa'
            };

            const result = query.filters(qs);

            expect(result).to.be.a('object');
            expect(result).to.have.property('active').to.be.a('object');
            expect(result.active).to.have.property('ee').to.be.a('string');
            expect(result).to.have.property('description').to.be.a('object');
            done();
        });

        it('should return object correct parsed and formated description', done => {
            const qs = {
                filters: 'active eq 1,document ee 12341234,description li *222*'
            };

            let result = query.filters(qs);

            expect(result).to.be.a('object');
            expect(result).to.have.property('active').to.be.a('object');
            expect(result).to.have.property('document').to.be.a('object');
            expect(result.document).to.have.property('ee').to.be.a('string');
            expect(result).to.have.property('description').to.be.a('object');
            expect(result.description[Op.like]).to.be.a('string').equal('%222%');

            result = query.filters.lastParser.format(result);

            expect(result).to.be.a('string').equal(qs.filters);
            done();
        });

        it('should return undefined when query string does not have filters', done => {
            const qs = {};

            const result = query.filters(qs);

            expect(result).to.be.a('undefined');
            done();
        });
    });

    describe('Order', () => {
        it('should return array with created_at DESC and id ASC', done => {
            const qs = {
                order: 'created_at-DESC,id-ASC'
            };

            const result = query.order(qs);

            expect(result).to.be.a('array');
            expect(result.length).to.be.a('number').equal(2);
            expect(result[0]).to.be.a('array');
            expect(result[0][0]).to.be.a('string').equal('created_at');
            expect(result[0][1]).to.be.a('string').equal('DESC');
            expect(result[1][0]).to.be.a('string').equal('id');
            expect(result[1][1]).to.be.a('string').equal('ASC');
            done();
        });

        it('should return undefined when query string does not have order', done => {
            const qs = {};

            const result = query.order(qs);

            expect(result).to.be.a('undefined');
            done();
        });
    });

    describe('Group', () => {
        it('should return array with id_token', done => {
            const qs = {
                group: 'id_token'
            };

            const result = query.group(qs);

            expect(result).to.be.a('array');
            expect(result.length).to.be.a('number').equal(1);
            expect(result[0]).to.be.a('string').equal('id_token');
            done();
        });

        it('should return undefined when query string does not have order', done => {
            const qs = {};

            const result = query.group(qs);

            expect(result).to.be.a('undefined');
            done();
        });
    });

    describe('Without', () => {
        it('should return object with "without" property', done => {
            const qs = {
                without: 'status,accounts'
            };

            const result = query.without(qs);

            expect(result).to.be.a('object').to.have.property('without').to.be.a('array');
            expect(result.without.length).to.be.a('number').equal(2);
            expect(result.without[0]).to.be.a('string').equal('status');
            expect(result.without[1]).to.be.a('string').equal('accounts');
            done();
        });

        it('should return undefined when query string does not have "without"', done => {
            const qs = {};

            const result = query.order(qs);

            expect(result).to.be.a('undefined');
            done();
        });

        it('should return object with "without" and relationships', done => {
            const qs = {
                without: 'status,accounts.wallet'
            };

            const result = query.without(qs);

            expect(result).to.be.a('object').to.have.property('without').to.be.a('array');
            expect(result).to.be.a('object').to.have.property('relationships').to.be.a('object');
            expect(result.without.length).to.be.a('number').equal(1);
            expect(result.without[0]).to.be.a('string').equal('status');
            expect(result.relationships).to.have.property('accounts').to.be.a('object');
            expect(result.relationships.accounts).to.have.property('without').to.be.a('array');
            expect(result.relationships.accounts.without.length).to.be.a('number').equal(1);
            expect(result.relationships.accounts.without[0]).to.be.a('string').equal('wallet');
            done();
        });
    });

    describe('With', () => {
        it('should return object with "with" property', done => {
            const qs = {
                with: 'status,accounts'
            };

            const result = query.with(qs);

            expect(result).to.be.a('object').to.have.property('with').to.be.a('array');
            expect(result.with.length).to.be.a('number').equal(2);
            expect(result.with[0]).to.be.a('string').equal('status');
            expect(result.with[1]).to.be.a('string').equal('accounts');
            done();
        });

        it('should return undefined when query string does not have "with"', done => {
            const qs = {};

            const result = query.order(qs);

            expect(result).to.be.a('undefined');
            done();
        });

        it('should return object with "with" and relationships', done => {
            const qs = {
                with: 'status,accounts.wallet'
            };

            const result = query.with(qs);

            expect(result).to.be.a('object').to.have.property('with').to.be.a('array');
            expect(result).to.be.a('object').to.have.property('relationships').to.be.a('object');
            expect(result.with.length).to.be.a('number').equal(1);
            expect(result.with[0]).to.be.a('string').equal('status');
            expect(result.relationships).to.have.property('accounts').to.be.a('object');
            expect(result.relationships.accounts).to.have.property('with').to.be.a('array');
            expect(result.relationships.accounts.with.length).to.be.a('number').equal(1);
            expect(result.relationships.accounts.with[0]).to.be.a('string').equal('wallet');
            done();
        });
    });

    describe('Hidden', () => {
        it('should return object with "hidden" property', done => {
            const qs = {
                hidden: 'status,accounts'
            };

            const result = query.hidden(qs);

            expect(result).to.be.a('object').to.have.property('hidden').to.be.a('array');
            expect(result.hidden.length).to.be.a('number').equal(2);
            expect(result.hidden[0]).to.be.a('string').equal('status');
            expect(result.hidden[1]).to.be.a('string').equal('accounts');
            done();
        });

        it('should return undefined when query string does not have "hidden"', done => {
            const qs = {};

            const result = query.order(qs);

            expect(result).to.be.a('undefined');
            done();
        });

        it('should return object hidden "hidden" and relationships', done => {
            const qs = {
                hidden: 'status,accounts.wallet'
            };

            const result = query.hidden(qs);

            expect(result).to.be.a('object').to.have.property('hidden').to.be.a('array');
            expect(result).to.be.a('object').to.have.property('relationships').to.be.a('object');
            expect(result.hidden.length).to.be.a('number').equal(1);
            expect(result.hidden[0]).to.be.a('string').equal('status');
            expect(result.relationships).to.have.property('accounts').to.be.a('object');
            expect(result.relationships.accounts).to.have.property('hidden').to.be.a('array');
            expect(result.relationships.accounts.hidden.length).to.be.a('number').equal(1);
            expect(result.relationships.accounts.hidden[0]).to.be.a('string').equal('wallet');
            done();
        });
    });
});
